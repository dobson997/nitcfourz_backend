<!DOCTYPE html>
<html>
  <head>
    @include('meta.meta')
    <title>NitcFourz Admin</title>
    <link rel="stylesheet" href="./apps/admin/vendor.css"/>
    <link rel="stylesheet" href="./apps/admin/styles.css"/>
    
  </head>
  <body ng-app="app">
    <div ui-view></div>
    <script src="./apps/admin/vendor.js"></script>
    <script src="./apps/admin/app.js"></script>
  </body>
</html>
