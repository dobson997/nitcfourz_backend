<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Language" content="en"/>
    <meta name="viewport" content="width=device-width"/>
    <meta property="og:title" content="{{$title}}"/>
    <meta property="og:site_name" content="NITC FOURZ FANTASY"/>
    <meta property="og:url" content="{{url('share/myteam/'.$id)}}"/>
    <meta property="og:description" content="Make your team at fourznitc.in"/>
    <meta property="og:type" content="article"/>
    <meta property="fb:app_id" content="1861675950771308"/>
    <meta property="og:image" content="{{url('share/image/myteam/'.$id)}}"/>
    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="630"/>
    <title>NITC FOURZ FANTASY</title>
    <link rel="stylesheet" href="./apps/frontend/vendor.css"/>
    <link rel="stylesheet" href="./apps/frontend/styles.css"/>
  </head>
  <body ng-app="app">
    <!-- view -->
    <script>
        window.location = "{{url('/')}}";
    </script>
  </body>
</html>
