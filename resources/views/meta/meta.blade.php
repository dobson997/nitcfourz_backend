    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Language" content="en"/>
    <meta name="viewport" content="width=device-width"/>
    <meta property="og:title" content="NITC FOURZ FANTASY"/>
    <meta property="og:site_name" content="fourznitc.in"/>
    <meta property="og:url" content="http://www.fourznitc.in"/>
    <meta property="og:description" content="Play fantasy fourz."/>
    <meta property="og:type" content="article"/>
    <meta property="fb:app_id" content="1861675950771308"/>
    <meta property="og:image" content="{{url('images/fantasy.jpg')}}"/>
    <meta property="og:image:width" content="1200"/>
    <meta property="og:image:height" content="630"/>
    <title>NITC FOURZ FANTASY</title>
