<!DOCTYPE html>
<html>
  <head>

    @include('meta.meta')

    <link rel="stylesheet" href="./apps/frontend/vendor.css"/>
    <link rel="stylesheet" href="./apps/frontend/styles.css"/>
  </head>
  <body ng-app="app">
    <div ui-view></div>
    <script src="./apps/frontend/vendor.js"></script>
    <script src="./apps/launch/game.js"></script>
  </body>
</html>
