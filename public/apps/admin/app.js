
/*console.log = function(){

};*/

(function() {

	'use strict';

	var app = angular.module('app', [
			'ui.router',
			'satellizer',
			'AppControllers',
            'AppDirectives',
            'AppFilters',
			'AppServices',
			'AppConfig',
			'lumx',
			'ngAnimate',
			'ngScrollbars',
			'AppRoutes'
	]);

	angular.module('AppControllers',[]);
	angular.module('AppDirectives',['lumx']);
	angular.module('AppServices',['ngResource']);
	angular.module('AppFilters',[]);
	angular.module('AppConfig',[]);
	angular.module('AppRoutes',[
		'MainRoutes',
		'PlayerRoutes',
		'GameweekRoutes',
        'FixtureRoutes',
        'StatusRoutes',
        'AdminRoutes',
        'TeamRoutes',
        'UserRoutes',
        'ClaimRoutes'
	]);

	app.config(function($stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide, Config) {

        function redirectWhenLoggedOut($q, $injector) {
            return {
                responseError: function(rejection) {

                    var $state = $injector.get('$state'); //can't inject state else circular dependency
                    var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];
                    angular.forEach(rejectionReasons, function(value, key) {
                        if(rejection.data.error === value) {
                            localStorage.removeItem('user');
                            $state.go('auth');
                        }
                    });
                    return $q.reject(rejection);
                }
            };
        }

        $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);
        $httpProvider.interceptors.push('redirectWhenLoggedOut');
        $authProvider.baseUrl = './';
        $authProvider.loginUrl = 'api/admin/authenticate';

        $urlRouterProvider.otherwise('/home');

	}).run(function($rootScope, $state, $auth, $window, $timeout, MainConfig, Middleware, AuthService) {


        AuthService.fbinit();

		$rootScope.user = {};
		$rootScope.go = function(state,params){
			$state.go(state,params);
		};

        $rootScope.$on('$stateChangeStart', function(event, toState) {

            console.log(toState);
            $("html, body").animate({scrollTop: 0}, "slow"); //not a good practice though!


            Middleware.auth($rootScope.authenticated,event,toState);

            //Middleware.fakeHelper(event, toState);

        });

        //Middleware.fakeAuth();
	});

})();

(function() {

	'use strict';

  var config = angular.module('AppConfig');
  config.constant('Config',{
	  templates: "./apps/admin/templates/",
	  partials: './templates/partials/',
      appId:    '1861675950771308' // '551333468325789' //  

  });

  config.factory('MainConfig',function($timeout, Config, BackendConfig) {

	  var backend;

	  var resolveConfig = function(cb) {
		  console.log('resolving..');
		  if(backend){
			  cb(true);
			  return;
		  }
		  BackendConfig.get(function(res) {
			  backend = res;
			  console.log(res);
    		  cb(true);
    	  }, function (err) {
    		  cb(false);
    	  });
	  };

	  var getBackend = function(){
		  return backend;
	  };

	  var setBackend = function(config){
		 backend = config;
	  };


	  var basicScrollConfig = {
            autoHideScrollbar: false,
            theme: 'dark',
            axis: 'y',
            advanced:{
                updateOnContentResize: true
            },
            setHeight: '80vh',
            scrollInertia: 500,
            mouseWheel:{
                scrollAmount: 250
            }
      };

	  var getScrollConfig = function(callback){
		  var config = angular.copy(basicScrollConfig);
	      config.callbacks = {
	          onTotalScroll : callback
	      };
		  return config;
	  };

	  var mainmenu = [
          {
              name: 'Users',
              link: 'user',
              parent : 'user',
              icon: 'user'
          },
          {
              name: 'Profile Claims',
              link: 'claim',
              parent : 'claim',
              icon: 'user'
          },
          {
              name: 'Create Teams',
              link: 'team',
              parent : 'team',
              icon: 'user'
          },
          {
              name: 'Create Players',
              link: 'player',
              parent : 'player',
              icon: 'user'
          },
		  {
              name: 'Add Gameweek Details',
              link: 'gameweek',
              parent : 'gameweek',

              icon: 'user'
          },
          {
              name: 'Fixtures',
              link: 'fixture',
              parent : 'fixture',

              icon: 'user'
          },
          {
              name: 'Status',
              link: 'status',
              parent : 'status',

              icon: 'user'
          },
          {
              name: 'Admins',
              link: 'admin',
              parent : 'admin',

              icon: 'user'
          },
          {
             name: 'Logout',
             link: 'logout',
             parent : 'logout',
             icon: 'sign-out'
         }
      ];

	  var logoutmenu = [{
          name: 'Login',
          link: 'auth',
          icon: 'lock'
      }];

	  return {
		  getScrollConfig: getScrollConfig,
		  basicScrollConfig : basicScrollConfig,
		  resolveConfig : resolveConfig,
		  getBackend : getBackend,
		  setBackend : setBackend,
		  mainmenu : mainmenu,
		  logoutmenu: logoutmenu
	  };


  });

})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('NavController', function($scope, $rootScope, $timeout, $state, MainConfig) {

      $scope.isOpen = false; //!
      $scope.$state = $state;
      $scope.menu = MainConfig.logoutmenu;

      var openedViaMenuButton = false;


      $scope.$on('onLoginComplete',function(e,args) {
          if($rootScope.user){
            console.log($rootScope.user);
            $scope.menu = MainConfig.mainmenu;
          }
      });

      $scope.menu = MainConfig.logoutmenu;

      $scope.$on('onLogoutComplete',function(e,args) {
          $scope.menu = MainConfig.logoutmenu;
      });

      $scope.nav = function(item){
        $scope.isOpen = false;
        if(openedViaMenuButton){
            $timeout(function(){
                $state.go(item.link);
            },1000);
        }else{
            $state.go(item.link);
        }
      };

      $scope.openMenu = function(){
          openedViaMenuButton = true;
          $scope.isOpen = !$scope.isOpen;
      };


  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('AdminAddController', function($scope, LBHandler, Admin, User) {

      $scope.handler = LBHandler.getHandler(1000);
      $scope.input = {};


      $scope.add = function(){
        $scope.errors = [];

        var model = $scope.input;
        model.fbid = $scope.input.selectFbUser.fbid;

        $scope.handler.update(Admin.create, model, function (res) {
            console.log(res);
            $scope.added = res;
            $scope.input = {};
            $scope.form.$setPristine();
            $scope.search();
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('AdminController', function($scope, MainConfig, Pagination,  Admin, $state, User) {

      User.select(function (res) {
          $scope.users = res;
          console.log(res);
      }, function(err) {
         console.log(err);
      });


      $scope.results = [];
      var paginator = Pagination.getPaginator(Admin.search, {term:''}, function() {
          $scope.results = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();
      $scope.scrollconfig = MainConfig.getScrollConfig(paginator.loadMore);

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      $scope.edit = function(c){
          $scope.to_edit = c;
          $state.go('admin.edit',{id:c.id});
      };

      $scope.view = function(c){
          $scope.to_view = c;
          $state.go('admin.view', {id:c.id});
      };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('AdminEditController', function($scope, LBHandler, Admin, $timeout) {

      if(!$scope.to_edit){
          $scope.go('admin.add',{});
      }

    //   $scope.handler = LBHandler.getHandler(2000);
    //   $scope.input = angular.copy($scope.to_edit);
      //
    //   $scope.save = function(){
    //     $scope.errors = [];
    //     var model = $scope.input;
    //     $scope.handler.update(Admin.post,model, function (res) {
    //         console.log(res);
    //         $scope.search();
    //         $scope.to_edit = res;
      //
    //     }, function (err) {
    //         $scope.errors = err.data.errors;
    //         console.log(err);
    //     });
      //
    //   };

      $scope.deleteHandler = LBHandler.getHandler(2000);

      $scope.delete = function(){
        $scope.deleteErrors = [];
        var model = {
            id: $scope.to_edit.fbid,
        };
        $scope.deleteHandler.update(Admin.delete, model, function (res) {
            console.log(res);
            $timeout(function(){
                $scope.search();
                $scope.go('admin');
            }, 2000);
        }, function (err) {
            $scope.deleteErrors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('AdminViewController', function($scope, $state, Admin, $stateParams) {

      var id = $stateParams.id;
      if(!$scope.to_view){
          Admin.get({id:id},function (res) {
                $scope.to_view = res;
            }, function (err) {
                $scope.errors = err.data.errors;
            });
      }

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('AuthController', function($rootScope, $scope, $state, AuthService, LBHandler, $auth, MainConfig) {

      $scope.loading = true;

      if(AuthService.getIsLoggedOut()){
          //came here by logging out.
          $scope.loading = false;
      }

      $scope.$on('onSdkLoad',function(){
          AuthService.getLoginStatus(function(data){

              if(!data.status){
                  console.log(data);
                  $scope.loading = false;
                  $scope.$apply();
              }
          });
      });

      $scope.$on('onLoginComplete',function(e,args) {
          console.log('Login complete. Going Home.');
          $state.go('home');
      });


      $scope.login = function() {
          AuthService.login(function(res){

          });
      };


  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('LogOutController', function($scope, AuthService, $state) {

      AuthService.logout(function() {
          $state.go('auth');
      });

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('ClaimAddController', function($scope, LBHandler, Claim) {

      $scope.handler = LBHandler.getHandler(1000);
      $scope.input = {};

      $scope.add = function(){
        $scope.errors = [];

        var model = $scope.input;

        $scope.handler.update(Claim.create, model, function (res) {
            console.log(res);
            $scope.added = res;
            $scope.input = {};
            $scope.form.$setPristine();
            $scope.search();
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('ClaimController', function($scope, MainConfig, Pagination,  Claim, $state) {

      $scope.results = [];
      var paginator = Pagination.getPaginator(Claim.search, {term:''}, function() {
          $scope.results = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();
      $scope.scrollconfig = MainConfig.getScrollConfig(paginator.loadMore);

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      $scope.edit = function(c){
          $scope.to_edit = c;
          $state.go('claim.edit',{id:c.id});
      };

      $scope.view = function(c){
          $scope.to_view = c;
          $state.go('claim.view', {id:c.id});
      };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('ClaimEditController', function($scope, LBHandler, Claim, $timeout) {

      if(!$scope.to_edit){
          $scope.go('employee.add',{});
      }

      $scope.handler = LBHandler.getHandler(2000);
      $scope.input = angular.copy($scope.to_edit);

      $scope.save = function(){
        $scope.errors = [];
        var model = $scope.input;
        $scope.handler.update(Claim.post,model, function (res) {
            console.log(res);
            $scope.search();
            $scope.to_edit = res;

        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

      $scope.deleteHandler = LBHandler.getHandler(2000);

      $scope.delete = function(){
        $scope.deleteErrors = [];
        var model = {
            id: $scope.to_edit.id,
        };
        $scope.deleteHandler.update(Claim.delete, model, function (res) {
            console.log(res);
            $timeout(function(){
                $scope.search();
                $scope.go('claim');
            }, 2000);
        }, function (err) {
            $scope.deleteErrors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('ClaimViewController', function($scope, $state, Claim, $stateParams, LBHandler) {

      var id = $stateParams.id;
      if(!$scope.to_view){
          Claim.get({id:id},function (res) {
                $scope.to_view = res;
            }, function (err) {
                $scope.errors = err.data.errors;
            });
      }

      $scope.handler = LBHandler.getHandler(2000);

      $scope.approve = function(state){
        $scope.errors = [];
        var model = angular.copy($scope.to_view);
        model.approve = state;
        $scope.handler.update(Claim.post,model, function (res) {
            console.log(res);
            $scope.to_view= res;
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('FixtureAddController', function($scope, LBHandler, Fixture) {

      $scope.handler = LBHandler.getHandler(1000);
      $scope.input = {};

      $scope.setTime = function () {
        $scope.timestring = moment($scope.input.time,"HH:mm").format();
      };

      $scope.pattern = /[0-2]\d:[0-6]\d/;

      $scope.add = function(){
        $scope.errors = [];

        var model = angular.copy($scope.input);
        model.team1 = $scope.input.selectTeam1.teamname;
        model.team2 = $scope.input.selectTeam2.teamname;
        model.time = $scope.timestring;
        console.log(model);
        $scope.handler.update(Fixture.create, model, function (res) {
            console.log(res);
            $scope.added = res;
            $scope.input.selectTeam1 = undefined;
            $scope.input.selectTeam2 = undefined;
            $scope.form.$setPristine();
            $scope.search();
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('FixtureController', function($scope, MainConfig, Pagination,  Fixture, $state, Player) {

      Player.teams(function (res) {
        $scope.teams = res;
        console.log(res);
      },function (err) {
            console.log(err);
      });

      $scope.results = [];
      var paginator = Pagination.getPaginator(Fixture.search, {term:''}, function() {
          $scope.results = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();
      $scope.scrollconfig = MainConfig.getScrollConfig(paginator.loadMore);

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      $scope.edit = function(c){
          $scope.to_edit = c;
          $state.go('fixture.edit',{id:c.id});
      };

      $scope.view = function(c){
          $scope.to_view = c;
          $state.go('fixture.view', {id:c.id});
      };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('FixtureEditController', function($scope, LBHandler, Fixture, $timeout) {

      if(!$scope.to_edit){
          $scope.go('fixture.add',{});
      }

      $scope.handler = LBHandler.getHandler(2000);
      $scope.input = angular.copy($scope.to_edit);
      $scope.input.selectTeam1 = {teamname:$scope.to_edit.team1};
      $scope.input.selectTeam2 = {teamname:$scope.to_edit.team2};

      $scope.input.date = moment($scope.to_edit.date).toDate();
      $scope.input.dateFormatted = moment($scope.to_edit.date).format('LL');
      $scope.input.time = moment($scope.to_edit.time).format('HH:mm');

      $scope.setTime = function () {
        $scope.timestring = moment($scope.input.time,"HH:mm").format();
      };

      $scope.setTime();

      $scope.pattern = /[0-2]\d:[0-6]\d/;

      $scope.save = function(){
        $scope.errors = [];
        var model = $scope.input;
        model.team1 = $scope.input.selectTeam1.teamname;
        model.team2 = $scope.input.selectTeam2.teamname;
        model.time = $scope.timestring;
        console.log(model);
        $scope.handler.update(Fixture.post,model, function (res) {
            console.log(res);
            $scope.search();
            $scope.to_edit = res;

        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

      $scope.deleteHandler = LBHandler.getHandler(2000);

      $scope.delete = function(){
        $scope.deleteErrors = [];
        var model = {
            id: $scope.to_edit.id,
        };
        $scope.deleteHandler.update(Fixture.delete, model, function (res) {
            console.log(res);
            $timeout(function(){
                $scope.search();
                $scope.go('fixture');
            }, 2000);
        }, function (err) {
            $scope.deleteErrors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('FixtureViewController', function($scope, $state, Fixture, $stateParams) {

      var id = $stateParams.id;
      if(!$scope.to_view){
          Fixture.get({id:id},function (res) {
                $scope.to_view = res;
            }, function (err) {
                $scope.errors = err.data.errors;
            });
      }

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('GameweekAddController', function($scope, LBHandler, Gameweek) {

      $scope.handler = LBHandler.getHandler(1000);
      $scope.input = {
          goals : 0,
          cleansheet : 0,
          save : 0,
          assist : 0,
          redcard : 0,
          yellowcard : 0,
          penaltysaved : 0,
          penaltymissed : 0
      };
      $scope.playtypes = [0,1];

      $scope.add = function(){
        $scope.errors = [];

        var model = $scope.input;
        model.playerid = $scope.input.selectPlayer.id;
        model.hasplayed = $scope.input.selectHasplayed;
        console.log(model);
        $scope.handler.update(Gameweek.create, model, function (res) {
            console.log(res);
            $scope.added = res;
            $scope.input.selectPlayer = undefined;
            $scope.form.$setPristine();
            $scope.search();
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('GameweekController', function($scope, MainConfig, Pagination,  Gameweek, $state, Player) {

      Player.select(function (res) {
          console.log(res);
          $scope.players = res;
      }, function (err) {
         console.log(err);
      });

      $scope.results = [];
      var paginator = Pagination.getPaginator(Gameweek.search, {term:''}, function() {
          $scope.results = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();
      $scope.scrollconfig = MainConfig.getScrollConfig(paginator.loadMore);

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      $scope.edit = function(c){
          $scope.to_edit = c;
          $state.go('gameweek.edit',{id:c.id});
      };

      $scope.view = function(c){
          $scope.to_view = c;
          $state.go('gameweek.view', {id:c.id});
      };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('GameweekEditController', function($scope, LBHandler, Gameweek, $timeout) {

      if(!$scope.to_edit){
          $scope.go('gameweek.add',{});
      }

      $scope.handler = LBHandler.getHandler(2000);
      $scope.input = angular.copy($scope.to_edit);
      $scope.input.selectPlayer = $scope.input.player;
      $scope.input.selectHasplayed = $scope.to_edit.hasplayed;

      $scope.playtypes = [0,1];

      $scope.save = function(){
        $scope.errors = [];
        var model = $scope.input;
        model.playerid = $scope.input.selectPlayer.id;
        model.hasplayed = $scope.input.selectHasplayed;
        console.log(model);
        $scope.handler.update(Gameweek.post,model, function (res) {
            console.log(res);
            $scope.search();
            $scope.to_edit = res;

        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

      $scope.deleteHandler = LBHandler.getHandler(2000);

      $scope.delete = function(){
        $scope.deleteErrors = [];
        var model = {
            id: $scope.to_edit.id,
        };
        $scope.deleteHandler.update(Gameweek.delete, model, function (res) {
            console.log(res);
            $timeout(function(){
                $scope.search();
                $scope.go('gameweek');
            }, 2000);
        }, function (err) {
            $scope.deleteErrors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('GameweekViewController', function($scope, $state, Gameweek, $stateParams) {

      var id = $stateParams.id;
      if(!$scope.to_view){
          Gameweek.get({id:id},function (res) {
                $scope.to_view = res;
            }, function (err) {
                $scope.errors = err.data.errors;
            });
      }

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('PlayerAddController', function($scope, LBHandler, Player, MainConfig, User) {

      $scope.handler = LBHandler.getHandler(1000);
      $scope.input = {};
      $scope.type = MainConfig.getBackend().player.types;

      $scope.add = function(){
        $scope.errors = [];

        var model = $scope.input;
        model.type = $scope.input.selectType.type;
        model.teamname = $scope.input.selectTeam.teamname;
        if($scope.input.selectFbUser){
            model.fbid = $scope.input.selectFbUser.fbid;
        }
        $scope.handler.update(Player.create, model, function (res) {
            console.log(res);
            $scope.added = res;
            $scope.input.name = undefined;
            $scope.input.price = undefined;
            $scope.form.$setPristine();
            $scope.search();
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('PlayerController', function($scope, MainConfig, Pagination,  Player, $state, User, Team) {

      User.select(function (res) {
          $scope.users = res;
          console.log(res);
      }, function(err) {
         console.log(err);
      });

      Team.getAll(function (res) {
         $scope.teams = res;
         console.log(res);
     },function (err) {
        console.log(err);
     });

      $scope.results = [];
      var paginator = Pagination.getPaginator(Player.search, {term:''}, function() {
          $scope.results = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();
      $scope.scrollconfig = MainConfig.getScrollConfig(paginator.loadMore);

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      $scope.edit = function(c){
          $scope.to_edit = c;
          $state.go('player.edit',{id:c.id});
      };

      $scope.view = function(c){
          $scope.to_view = c;
          $state.go('player.view', {id:c.id});
      };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('PlayerEditController', function($scope, LBHandler, Player, $timeout, MainConfig, User) {


      if(!$scope.to_edit){
          $scope.go('player.add',{});
      }

      $scope.handler = LBHandler.getHandler(2000);
      $scope.input = angular.copy($scope.to_edit);
      $scope.type = MainConfig.getBackend().player.types;
      $scope.input.selectFbUser = $scope.to_edit;
      $scope.input.selectTeam = {
          teamname : $scope.to_edit.teamname
      };
      $scope.input.selectType = {
          option : MainConfig.getBackend().playerTypes[$scope.to_edit.type],
          type : $scope.to_edit.type
      };

      $scope.save = function(){
        $scope.errors = [];
        var model = $scope.input;
        model.type = $scope.input.selectType.type;
        model.teamname = $scope.input.selectTeam.teamname;
        if($scope.input.selectFbUser){
            model.fbid = $scope.input.selectFbUser.fbid;
        }
        $scope.handler.update(Player.post,model, function (res) {
            console.log(res);
            $scope.search();
            $scope.to_edit = res;

        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

      $scope.deleteHandler = LBHandler.getHandler(2000);

      $scope.delete = function(){
        $scope.deleteErrors = [];
        var model = {
            id: $scope.to_edit.id,
        };
        $scope.deleteHandler.update(Player.delete, model, function (res) {
            console.log(res);
            $timeout(function(){
                $scope.search();
                $scope.go('player');
            }, 2000);
        }, function (err) {
            $scope.deleteErrors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('PlayerViewController', function($scope, $state, Player, $stateParams) {

      var id = $stateParams.id;
      if(!$scope.to_view){
          Player.get({id:id},function (res) {
                $scope.to_view = res;
            }, function (err) {
                $scope.errors = err.data.errors;
            });
      }

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('StatusController', function($scope, MainConfig, Pagination,  Status, $state, LBHandler, Playerhistory, Gameweek) {

        $scope.status = {};
        Status.get(function (res) {
            $scope.status = res;
            $scope.input = res;
            $scope.input.selectGame_state = {
                    option : MainConfig.getBackend().gameStates[res.game_state],
                    type : res.game_state
            };
            $scope.input.selectTransfer_allowed = {
                    option : MainConfig.getBackend().transferTypes[res.transfer_allowed],
                    type : res.transfer_allowed
            };
            console.log(res);
        }, function (err) {
            console.log(err);
        });

        $scope.handler = LBHandler.getHandler(1000);
        $scope.gameStates = MainConfig.getBackend().game.states;
        $scope.transferTypes = MainConfig.getBackend().transfer.types;


        $scope.update = function () {
            $scope.errors = undefined;
            var model = $scope.status;
            model.transfer_allowed = $scope.input.selectTransfer_allowed.type;
            model.game_state = $scope.input.selectGame_state.type;

                $scope.handler.update(Status.update, model, function () {

                },function (err) {
                    $scope.errors = err.data.errors;
                    console.log(err);
                });
        };

        $scope.updatehandler = LBHandler.getHandler(1000);
        $scope.updatePH = function(){
                $scope.errorsPH = undefined;
                $scope.updatehandler.update(Playerhistory.update, {}, function(res) {
                    console.log(res);
                }, function (err) {
                    $scope.errorsPH = err.data.errors;
                    console.log(err);
                });
        };
        $scope.updatehandler2 = LBHandler.getHandler(1000);
        $scope.copyTemporary = function () {
            $scope.errorsCopy = undefined;
            $scope.updatehandler2.update(Playerhistory.copy, {}, function(res) {
                console.log(res);

            }, function (err) {
                console.log(err);
                $scope.errorsCopy = err.data.errors;
            });
        };


        $scope.updatehandler3 = LBHandler.getHandler(1000);
        $scope.autoFill = function () {
            $scope.errorsAutofill = undefined;
            $scope.updatehandler3.update(Gameweek.autofill, {}, function(res) {
                console.log(res);

            }, function (err) {
                console.log(err);
                $scope.errorsAutofill = err.data.errors;
            });
        };


  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('TeamAddController', function($scope, LBHandler, Team) {

      $scope.handler = LBHandler.getHandler(1000);
      $scope.input = {};

      $scope.add = function(){
        $scope.errors = [];

        var model = $scope.input;

        $scope.handler.update(Team.create, model, function (res) {
            console.log(res);
            $scope.added = res;
            $scope.input = {};
            $scope.form.$setPristine();
            $scope.search();
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('TeamController', function($scope, MainConfig, Pagination,  Team, $state) {

      $scope.results = [];
      var paginator = Pagination.getPaginator(Team.search, {term:''}, function() {
          $scope.results = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();
      $scope.scrollconfig = MainConfig.getScrollConfig(paginator.loadMore);

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      $scope.edit = function(c){
          $scope.to_edit = c;
          $state.go('team.edit',{id:c.id});
      };

      $scope.view = function(c){
          $scope.to_view = c;
          $state.go('team.view', {id:c.id});
      };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('TeamEditController', function($scope, LBHandler, Team, $timeout) {

      if(!$scope.to_edit){
          $scope.go('employee.add',{});
      }

      $scope.handler = LBHandler.getHandler(2000);
      $scope.input = angular.copy($scope.to_edit);

      $scope.save = function(){
        $scope.errors = [];
        var model = $scope.input;
        $scope.handler.update(Team.post,model, function (res) {
            console.log(res);
            $scope.search();
            $scope.to_edit = res;

        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

      $scope.deleteHandler = LBHandler.getHandler(2000);

      $scope.delete = function(){
        $scope.deleteErrors = [];
        var model = {
            id: $scope.to_edit.id,
        };
        $scope.deleteHandler.update(Team.delete, model, function (res) {
            console.log(res);
            $timeout(function(){
                $scope.search();
                $scope.go('team');
            }, 2000);
        }, function (err) {
            $scope.deleteErrors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('TeamViewController', function($scope, $state, Team, $stateParams) {

      var id = $stateParams.id;
      Team.get({id:id},function (res) {
            $scope.players = res.players;
        }, function (err) {
            $scope.errors = err.data.errors;
       });

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('UserAddController', function($scope, LBHandler, User) {

      $scope.handler = LBHandler.getHandler(1000);
      $scope.input = {};

      $scope.add = function(){
        $scope.errors = [];

        var model = $scope.input;

        $scope.handler.update(User.create, model, function (res) {
            console.log(res);
            $scope.added = res;
            $scope.input = {};
            $scope.form.$setPristine();
            $scope.search();
        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('UserController', function($scope, MainConfig, Pagination,  User, $state) {

      $scope.results = [];
      var paginator = Pagination.getPaginator(User.search, {term:''}, function() {
          $scope.results = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();
      $scope.scrollconfig = MainConfig.getScrollConfig(paginator.loadMore);

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      $scope.edit = function(c){
          $scope.to_edit = c;
          $state.go('user.edit',{id:c.id});
      };

      $scope.view = function(c){
          $scope.to_view = c;
          $state.go('user.view', {id:c.id});
      };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('UserEditController', function($scope, LBHandler, User, $timeout) {

      if(!$scope.to_edit){
          $scope.go('employee.add',{});
      }

      $scope.handler = LBHandler.getHandler(2000);
      $scope.input = angular.copy($scope.to_edit);

      $scope.save = function(){
        $scope.errors = [];
        var model = $scope.input;
        $scope.handler.update(User.post,model, function (res) {
            console.log(res);
            $scope.search();
            $scope.to_edit = res;

        }, function (err) {
            $scope.errors = err.data.errors;
            console.log(err);
        });

      };

      $scope.deleteHandler = LBHandler.getHandler(2000);

      $scope.delete = function(){
        $scope.deleteErrors = [];
        var model = {
            id: $scope.to_edit.id,
        };
        $scope.deleteHandler.update(User.delete, model, function (res) {
            console.log(res);
            $timeout(function(){
                $scope.search();
                $scope.go('user');
            }, 2000);
        }, function (err) {
            $scope.deleteErrors = err.data.errors;
            console.log(err);
        });

      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('UserViewController', function($scope, $state, User, $stateParams) {

      var id = $stateParams.id;
      if(!$scope.to_view){
          User.get({id:id},function (res) {
                $scope.to_view = res;
            }, function (err) {
                $scope.errors = err.data.errors;
            });
      }

  });
})();


(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('dateandtime', function(Config){
            return {
                restrict: 'AEC',
                scope:{
                    datetime : '='
                },
                link: function(scope, element, attr) {
                    scope.in = {};
                    scope.$watch('datetime', function(newValue, oldValue) {
                        if(scope.datetime){
                            var mom = moment(scope.datetime);
                            scope.in = {
                                date : mom.toDate(),
                                time : mom.format('HH:mm'),
                                dateFormatted: mom.format('LL')
                            };

                        }
                    });



                    scope.pattern = /[0-2]\d:[0-6]\d/;


                    scope.change = function (val) {
                        if(!val) return;
                      var date = moment(scope.in.date).format('YYYY-MM-DD');
                      var f = date+" "+scope.in.time;
                      var m = moment(f,"YYYY-MM-DD HH:mm");
                      scope.datetime = m.format();
                    };
                },
                templateUrl: Config.templates+'directives/dateandtime.html'
            };
        });

})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('fbpic', function(Config){
            return {
                restrict: 'AEC',
                scope:{
                    fbid : '=',
                    dimension : '=',

                },
                link: function(scope, element, attr) {

                },
                templateUrl: Config.templates+'directives/fbpic.html'
            };
        });

})();

(function()
{
    'use strict';

    angular
        .module('AppDirectives')
        .directive('loadingButton', function(Config){
            return {
                restrict: 'AEC',
                scope:{
                    handler: '=',
                    disableCriteria: '=',
                    lxType : '@',
                    lxSize : '@',
                    lxDiameter : '@',
                    lxColor: '@',
                    value : '@',
                    successText : '@',
                    failText : '@'
                },
                link: function(scope, element, attr) {
                    if(scope.handler){
                        scope.$watch(function(scope){
                            return scope.handler.getState();
                        },function(newValue, oldValue){
                            scope.state = newValue;
                        });
                    }
                },
                templateUrl: Config.templates+'directives/loadingbutton.html'
            };
        });

})();

(function()
{
    'use strict';

    angular
        .module('AppDirectives')
        .directive('appProgress', lxProgress); /*check*/

    function lxProgress(Config)
    {
        return {
            restrict: 'E',
            templateUrl: Config.templates+'directives/progress.html',
            scope:
            {
                lxColor: '@?',
                lxDiameter: '@?',
                lxType: '@',
                lxValue: '@'
            },
            controller: LxProgressController,
            controllerAs: 'lxProgress',
            bindToController: true,
            replace: true
        };
    }

    function LxProgressController()
    {
        var lxProgress = this;
        init();
        lxProgress.getCircularProgressValue = getCircularProgressValue;
        lxProgress.getLinearProgressValue = getLinearProgressValue;
        lxProgress.getProgressDiameter = getProgressDiameter;



        ////////////
        
        function getCircularProgressValue()
        {
            if (angular.isDefined(lxProgress.lxValue))
            {
                return {
                    'stroke-dasharray': lxProgress.lxValue * 1.26 + ',200'
                };
            }
        }

        function getLinearProgressValue()
        {
            if (angular.isDefined(lxProgress.lxValue))
            {
                return {
                    'transform': 'scale(' + lxProgress.lxValue / 100 + ', 1)'
                };
            }
        }

        function getProgressDiameter()
        {
            if (lxProgress.lxType === 'circular')
            {
                return {
                    'transform': 'scale(' + parseInt(lxProgress.lxDiameter) / 100 + ')'
                };
            }

            return;
        }

        function init()
        {
            lxProgress.lxDiameter = angular.isDefined(lxProgress.lxDiameter) ? lxProgress.lxDiameter : 100;
            lxProgress.lxColor = angular.isDefined(lxProgress.lxColor) ? lxProgress.lxColor : 'primary';
        }
    }
})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('stringToNumber', function(){
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, ngModel) {
                  ngModel.$parsers.push(function(value) {
                    return '' + value;
                  });
                  ngModel.$formatters.push(function(value) {
                    return parseFloat(value);
                  });
                }
            };
        });

})();


(function(){
    'use strict';
    angular.module('AppFilters')
       .filter('gamestate',function(MainConfig){
            return function(input){
                var v = MainConfig.getBackend().gameStates[input];
                if(v){
                    return v;
                }else{
                    return 'Wrong State!';
                }
            };
        });
})();

(function(){
    'use strict';
    angular.module('AppFilters')
       .filter('hasplayedfilter',function(){
            return function(input){
                return input == 1 ? 'Played' : 'Not Played';
            };
        });
})();

(function(){
    'use strict';
    angular.module('AppFilters')
       .filter('playertype',function(MainConfig){
            return function(input){
                var v = MainConfig.getBackend().playerTypes[input];
                if(v){
                    return v;
                }else{
                    return 'Wrong Type!';
                }
            };
        });
})();

(function(){
    'use strict';
    angular.module('AppFilters')
       .filter('transferfilter',function(MainConfig){
           return function(input){
               var v = MainConfig.getBackend().transferTypes[input];
               if(v){
                   return v;
               }else{
                   return 'Wrong Type!';
               }
           };
        });
})();


(function() {
  'use strict';
  var base = './api/admin'; //<---

  var services = angular.module('AppServices');


  services.factory('Admin', function($resource) {
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/claim'; //<---

  var services = angular.module('AppServices');


  services.factory('Claim', function($resource) {
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/config/admin';

  var services = angular.module('AppServices');


  services.factory('BackendConfig', function($resource) {
    return $resource(base, {}, {
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/fixtures'; //<---

  var services = angular.module('AppServices');


  services.factory('Fixture', function($resource) {
    return $resource('./api/', {id:'@id', date: '@date'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        days : {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/days'
        },
        fixture : {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/fixture/:date'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/gameweek'; //<---

  var services = angular.module('AppServices');


  services.factory('Gameweek', function($resource) {
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        },
        autofill: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/autofill'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/player'; //<---

  var services = angular.module('AppServices');


  services.factory('Player', function($resource) {
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        select: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/select'
        },
        teams: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/teams'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/playerhistory'; //<---

  var services = angular.module('AppServices');


  services.factory('Playerhistory', function($resource) {
    return $resource('./api/', {id:'@id', userid:'@userid', weekid: '@weekid'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        weekpoints: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/weekpoints/:userid/:weekid'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        },
        update: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/update'
        },
        copy: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/copy'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/status'; //<---

  var services = angular.module('AppServices');


  services.factory('Status', function($resource) {
    return $resource('./api/', {id:'@id'}, {

        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/get'
        },
        update: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/update'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/team'; //<---

  var services = angular.module('AppServices');


  services.factory('Team', function($resource) {
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/user';

  var services = angular.module('AppServices');


  services.factory('User', function($resource) {
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        select: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/select'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        },
        noncreated:{
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/noncreated'
        }
    });
  });
})();


(function() {

	'use strict';
	var app = angular.module('AdminRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('admin', {
				url: '/admin',
				parent: 'app',
				templateUrl: Config.templates + 'admin/index.html',
				controller: 'AdminController'
			})
			.state('admin.add', {
				url: '/add',
				templateUrl: Config.templates + 'admin/add.html',
				controller: 'AdminAddController'
			})
			.state('admin.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'admin/edit.html',
				controller: 'AdminEditController'
			})
			.state('admin.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'admin/view.html',
				controller: 'AdminViewController'
			});

  });

})();

(function() {

	'use strict';
	var app = angular.module('ClaimRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('claim', {
				url: '/claim',
				parent: 'app',
				templateUrl: Config.templates + 'claim/index.html',
				controller: 'ClaimController'
			})
			.state('claim.add', {
				url: '/add',
				templateUrl: Config.templates + 'claim/add.html',
				controller: 'ClaimAddController'
			})
			.state('claim.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'claim/edit.html',
				controller: 'ClaimEditController'
			})
			.state('claim.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'claim/view.html',
				controller: 'ClaimViewController'
			});

  });

})();

(function() {

	'use strict';
	var app = angular.module('FixtureRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('fixture', {
				url: '/fixture',
				parent: 'app',
				templateUrl: Config.templates + 'fixture/index.html',
				controller: 'FixtureController'
			})
			.state('fixture.add', {
				url: '/add',
				templateUrl: Config.templates + 'fixture/add.html',
				controller: 'FixtureAddController'
			})
			.state('fixture.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'fixture/edit.html',
				controller: 'FixtureEditController'
			})
			.state('fixture.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'fixture/view.html',
				controller: 'FixtureViewController'
			});

  });

})();

(function() {

	'use strict';
	var app = angular.module('GameweekRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('gameweek', {
				url: '/gameweek',
				parent: 'app',
				templateUrl: Config.templates + 'gameweek/index.html',
				controller: 'GameweekController'
			})
			.state('gameweek.add', {
				url: '/add',
				templateUrl: Config.templates + 'gameweek/add.html',
				controller: 'GameweekAddController'
			})
			.state('gameweek.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'gameweek/edit.html',
				controller: 'GameweekEditController'
			})
			.state('gameweek.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'gameweek/view.html',
				controller: 'GameweekViewController'
			});

  });

})();

(function() {

	'use strict';
	var app = angular.module('MainRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
	        .state('app', {
	            abstract: true,
	            url: '',
	            templateUrl: Config.templates + 'base.html',
	            controller: 'NavController'
	        });

        $stateProvider
            .state('home', {
                url: '/home',
                parent : 'app',
                templateUrl: Config.templates + 'home.html',
            })

			.state('auth', {
	            url: '/login',
	            parent: 'app',
	            templateUrl: Config.templates + 'login.html',
	            controller: 'AuthController'
	        })

			.state('logout', {
	            url: '/logout',
	            parent: 'app',
	            template: '',
	            controller: 'LogOutController'
	        });
  });

})();

(function() {

	'use strict';
	var app = angular.module('PlayerRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('player', {
				url: '/player',
				parent: 'app',
				templateUrl: Config.templates + 'player/index.html',
				controller: 'PlayerController'
			})
			.state('player.add', {
				url: '/add',
				templateUrl: Config.templates + 'player/add.html',
				controller: 'PlayerAddController'
			})
			.state('player.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'player/edit.html',
				controller: 'PlayerEditController'
			})
			.state('player.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'player/view.html',
				controller: 'PlayerViewController'
			});

  });

})();

(function() {

	'use strict';
	var app = angular.module('StatusRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('status', {
				url: '/status',
				parent: 'app',
				templateUrl: Config.templates + 'status/index.html',
				controller: 'StatusController'
			});
			/*.state('status.add', {
				url: '/add',
				templateUrl: Config.templates + 'status/add.html',
				controller: 'StatusAddController'
			})
			.state('status.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'status/edit.html',
				controller: 'StatusEditController'
			})
			.state('status.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'status/view.html',
				controller: 'StatusViewController'
			});

            views: {
              'current_gw': {
                templateUrl: Config.templates + 'status/views/current_gw.html'
              },
              'deadline_gw': {
                templateUrl: Config.templates + 'status/views/deadline_gw.html'
              },
              'transfer_deadline': {
                templateUrl: Config.templates + 'status/views/transfer_deadline.html'
              },
              'transfer_allowed': {
                templateUrl: Config.templates + 'status/views/transfer_allowed.html'
              },
              'game_status': {
                templateUrl: Config.templates + 'status/views/game_status.html'
              },
              'launch_time': {
                templateUrl: Config.templates + 'status/views/launch_time.html'
              }
            }


            */

  });

})();

(function() {

	'use strict';
	var app = angular.module('TeamRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('team', {
				url: '/team',
				parent: 'app',
				templateUrl: Config.templates + 'team/index.html',
				controller: 'TeamController'
			})
			.state('team.add', {
				url: '/add',
				templateUrl: Config.templates + 'team/add.html',
				controller: 'TeamAddController'
			})
			.state('team.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'team/edit.html',
				controller: 'TeamEditController'
			})
			.state('team.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'team/view.html',
				controller: 'TeamViewController'
			});

  });

})();

(function() {

	'use strict';
	var app = angular.module('UserRoutes', []);
	app.config(function($stateProvider, Config) {

        $stateProvider
			.state('user', {
				url: '/user',
				parent: 'app',
				templateUrl: Config.templates + 'user/index.html',
				controller: 'UserController'
			})
			.state('user.add', {
				url: '/add',
				templateUrl: Config.templates + 'user/add.html',
				controller: 'UserAddController'
			})
			.state('user.edit', {
				url: '/edit/:id',
				templateUrl: Config.templates + 'user/edit.html',
				controller: 'UserEditController'
			})
			.state('user.view', {
				url: '/view/:id',
				templateUrl: Config.templates + 'user/view.html',
				controller: 'UserViewController'
			});

  });

})();


(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('LBHandler', function($timeout){
            var Handler = function(delay){
                var handler = this;
                handler.delay = delay;
                handler.state = 0; //0 - start, 1 - loading, 2 - success, 3 - failed
                handler.update = function(f,data,cb,errCb){
                    handler.state = 1;
                    f(data,function(res){
                        handler.state = 2;
                        handler.reset();
                        console.log(res);
                        cb(res);
                    },function(err){
                        handler.state = 3;
                        handler.reset();
                        console.log(err);
                        errCb(err);
                    });
                };

                handler.setState = function(state){
                    handler.state = state;
                };

                handler.reset = function(){
                    console.log('delay'+handler.delay);
                        $timeout(function () {
                            handler.setState(0);
                        }, handler.delay);
                };

                handler.getState = function(){
                    return handler.state;
                };

            };


            return {
                getHandler: function(delay){
                    return new Handler(delay);
                }
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('Middleware', function($rootScope, $timeout, $state, MainConfig, $stateParams){

			var auth = function(isAuth, event, toState){

				if(isAuth){
					if(toState.name === 'auth') {
						event.preventDefault();
						$state.go('home');
					}
				}else{
					if(toState.name !== 'auth'){
						event.preventDefault();
						$state.go('auth');
					}
				}
			};


            var fakeAuth = function(){
                $rootScope.authenticated = true;
                $rootScope.user = {id: 1, name:'Anas M', liquidcash : 10000, fbid: 4,
                    team : [
                        {
                          "id": 1,
                          "name": "Qf7JzjeJ9f",
                          "price": 1000,
                          "type": 2,
                          "teamname": "RamananFC",
                          "fbid": "mM1yHu4l0m"
                          }
                    ]
                };
                $timeout(function() {
                    console.log('Login');
                    $rootScope.$broadcast('onLoginComplete',{});
                },1500);
            };



            var fakeHelper = function(event,toState){

                if(!MainConfig.getBackend()){
    				event.preventDefault();
    				MainConfig.resolveConfig(function(success){
    					if(success)

    						$state.go(toState.name);

    				});
    			}else{
    				console.log(MainConfig.backend);
    			}
            };

            return {
				auth : auth,
                fakeHelper : fakeHelper,
                fakeAuth : fakeAuth
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('Pagination', function($timeout){

            var Paginator = function(pagefunc, params, success, fail){
                var paginator = this;

                paginator.reset = function(params){
                    paginator.page = 1;
                    paginator.results = [];
                    paginator.details = {};
                    paginator.params = params;
                };
                paginator.isLoading = false;
                paginator.reset(params);

                paginator.loadMore = function(){

                    if(paginator.isLoading){
                        return;
                    }

                    paginator.isLoading = true;
                    paginator.params.page = paginator.page;

                    pagefunc(paginator.params,function(response){
                        paginator.details = response;
                        console.log(paginator.page);
                        paginator.results = paginator.results.concat(response.data);
                        if(response.data.length>0){
                          paginator.page++;
                        }
                        paginator.isLoading = false;
                        console.log(response);
                        success(paginator.results, paginator.details);
                    },function(err){
                        console.log(err);
                        paginator.isLoading = false;
                        fail(err);
                    });
                };
            };
            return {
                getPaginator: function(pagefunc, success, fail){
                    return new Paginator(pagefunc, success, fail);
                }
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');

    services.factory('AuthFactory', function($resource) {
      return $resource('./api/', {}, {
          refreshToken: {
              method: 'POST',
              cache: false,
              isArray: false,
              url: 'api/refreshtoken'
          },
          password: {
              method: 'POST',
              cache: false,
              isArray: false,
              url: 'api/password'
          }
      });
    });


    services.factory('AuthService', function($rootScope,$auth,$state,$http, Config, MainConfig, $window){

    	var isLogggedOut = false;
        var explicitLogOut = false;

        var fbinit = function(){
            $window.fbAsyncInit = function() {

               FB.init({
                 appId: Config.appId,
                 status: true,
                 cookie: true,
                 xfbml: true,
                 version: 'v2.4'
                   });
                   watchLoginChange();
                   sdkLoaded();
            };

             (function(d){
               var js,
               id = 'facebook-jssdk',
               ref = d.getElementsByTagName('script')[0];
               if (d.getElementById(id)) {
                 return;
               }
               js = d.createElement('script');
               js.id = id;
               js.async = true;
               js.src = "//connect.facebook.net/en_US/all.js";
               ref.parentNode.insertBefore(js, ref);
             }(document));

        };

        var watchLoginChange = function() {
          FB.Event.subscribe('auth.authResponseChange', function(res) {
            if (res.status === 'connected') {
              console.log('connected');
    		  getUserInfo(function(res,token){

    			var credentials = {
    					 fb_token: token
    			 };

    			$auth
    			 .login(credentials)
    			 .then(function(response) {

    					isLogggedOut = false;
    					var data = response.data;
    					$auth.setToken(data.token);
    					$http.defaults.headers.common.Authorization = 'Bearer '+data.token;
    					$rootScope.authenticated = true;
    					$rootScope.user = data.user;
                        MainConfig.setBackend(data.config);

    					$rootScope.$broadcast('onLoginComplete',{});

    				 }, function(error) {
    					 console.log(error);
    					 $rootScope.authenticated = false;
    					 $rootScope.user = null;
    					 $auth.removeToken();
    			 });

    		  });

            }else {
              //not logged
              //console.log('not logged');

            }
          });
        };

    	var sdkLoaded = function(){
    		$rootScope.$broadcast('onSdkLoad',{});
    	};

    	var getLoginStatus = function(callback){
    		var data = {};
    		data.status = false;
    		FB.getLoginStatus(function(response) {
    		  if (response.status === 'connected') {
    		    	data.userid = response.authResponse.userID;
    		    	data.accessToken = response.authResponse.accessToken;
    				data.status = true;
    				callback(data);
    		  } else if (response.status === 'not_authorized') {
    			    callback(data);
    		  } else {
    			    callback(data);
    		  }
    		 });
    	};

        var getUserInfo = function(callback) {
          FB.api('/me', function(res) {
            var token =  FB.getAuthResponse().accessToken;
            $rootScope.$apply(function() {
              $rootScope.user = res;
              callback(res,token);
            });
          });
        };

    	var login = function(cb){
    		FB.login(function(response) {
    		    //let event system take care of login flow.
                console.log(response);
    		},{scope: 'email'});
    	};

        var logout = function(cb) {

          FB.logout(function(response) {
            $rootScope.$apply(function() {
    		  isLogggedOut = true;
              explicitLogOut = true;
    		  $http.defaults.headers.common.Authorization = '';
    		  $rootScope.authenticated = false;
      		  $rootScope.user = null;
      		  $auth.removeToken();
              $rootScope.$broadcast('onLogoutComplete',{});
    		  cb();
            });
          });
        };

    	var getIsLoggedOut = function(){
    		return isLogggedOut;
    	};

        var getExplicitLogOut = function(){
            return explicitLogOut;
        };


        return {
          watchLoginChange: watchLoginChange,
          getUserInfo: getUserInfo,
    	  getLoginStatus:getLoginStatus,
          logout: logout,
    	  login: login,
    	  sdkLoaded: sdkLoaded,
          fbinit: fbinit,
    	  getIsLoggedOut : getIsLoggedOut,
          getExplicitLogOut : getExplicitLogOut
        };
      });


	/*services.factory('AuthService', function($rootScope, Config, $auth, $state, $http, AuthFactory){
            var isLogggedOut = true;
            var explicitLogOut = false;

            var fbinit = function(){
                $window.fbAsyncInit = function() {

                   FB.init({
                     appId: config.appId,
                     status: true,
                     cookie: true,
                     xfbml: true
                       });
                       AuthService.watchLoginChange();
                       AuthService.sdkLoaded();
                };

                 (function(d){
                   var js,
                   id = 'facebook-jssdk',
                   ref = d.getElementsByTagName('script')[0];
                   if (d.getElementById(id)) {
                     return;
                   }
                   js = d.createElement('script');
                   js.id = id;
                   js.async = true;
                   js.src = "//connect.facebook.net/en_US/all.js";
                   ref.parentNode.insertBefore(js, ref);
                 }(document));

            };

            var login = function(credentials, success, fail){

                $auth
                 .login(credentials)
                 .then(function(response){
                        console.log(response);
                        isLogggedOut = false;
                        var data = response.data;
                        $auth.setToken(data.token);
                        $http.defaults.headers.common.Authorization = 'Bearer '+data.token;
                        $rootScope.authenticated = true;
                        $rootScope.user = data.user;
                        $rootScope.$broadcast('onLoginComplete',{});
                        success(data);

                     }, function(error) {
                         console.log(error);
                         $rootScope.authenticated = false;
                         $rootScope.user = null;
                         $auth.removeToken();
                         fail(error);
                 });

            };

            var autoLogin = function(success, fail) {
                    var token = $auth.getToken();
                    $http.defaults.headers.common.Authorization = 'Bearer '+token;
                    AuthFactory.refreshToken(function(res){
                        isLogggedOut = false;
                        var data = res; // we are using ngResource here!
                        $auth.setToken(data.token);
                        $http.defaults.headers.common.Authorization = 'Bearer '+data.token;
                        $rootScope.authenticated = true;
                        $rootScope.user = res.user;
                        $rootScope.$broadcast('onLoginComplete',{});
                        success(data);
                    },function(err){
                        console.log(err);
                        $rootScope.authenticated = false;
                        $rootScope.user = null;
                        $auth.removeToken();
                        fail(false);
                    });
            };

            var logout = function(cb) {
                isLogggedOut = true;
                explicitLogOut = true;
                $http.defaults.headers.common.Authorization = '';
                $rootScope.authenticated = false;
                $rootScope.user = null;
                $auth.removeToken();
                $rootScope.$broadcast('onLogoutComplete',{});
                cb();
            };

            var getExplicitLogOut = function(){
                return explicitLogOut;
            };

            var isLoggedIn = function(){

            };

            return {
                login: login,
                autoLogin : autoLogin,
                logout : logout,
                getExplicitLogOut : getExplicitLogOut
            };
    });*/


})();
