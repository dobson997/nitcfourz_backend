
/*console.log = function(){

};*/

(function() {

	'use strict';

	var app = angular.module('app', [
			'ui.router',
			'satellizer',
			'AppControllers',
            'AppDirectives',
            'AppFilters',
			'AppServices',
			'AppConfig',
			'lumx',
			'AppRoutes'
	]);

	angular.module('AppControllers',[]);
	angular.module('AppDirectives',['lumx']);
	angular.module('AppServices',['ngResource']);
	angular.module('AppFilters',[]);
	angular.module('AppConfig',[]);
	angular.module('AppRoutes',[
		'MainRoutes',
	]);

	app.config(function($stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide, Config) {

        "ngInject";

        function redirectWhenLoggedOut($q, $injector) {
            return {
                responseError: function(rejection) {

                    var $state = $injector.get('$state'); //can't inject state else circular dependency
                    var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];
                    angular.forEach(rejectionReasons, function(value, key) {
                        if(rejection.data.error === value) {
                            localStorage.removeItem('user');
                            $state.go('auth');
                        }
                    });
                    return $q.reject(rejection);
                }
            };
        }
        /* check */
        $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);
        $httpProvider.interceptors.push('redirectWhenLoggedOut');
        $authProvider.baseUrl = './';
        $authProvider.loginUrl = 'api/fbauthenticate';

        $urlRouterProvider.otherwise('/ready');


	}).run(function($rootScope, $state, $auth, $window, $timeout, MainConfig, Middleware, AuthService) {

        "ngInject";

        AuthService.fbinit();

		$rootScope.user = {};
		$rootScope.go = function(state,params){
			$state.go(state,params);
		};

        $rootScope.$on('$stateChangeStart', function(event, toState) {

            console.log(toState);
            $("html, body").animate({scrollTop: 0}, "slow"); //not a good practice though!

            Middleware.auth($rootScope.authenticated,event,toState);
            //Middleware.fakeHelper(event, toState);

        });

        //Middleware.fakeAuth();

	});

})();

(function() {

	'use strict';

  var config = angular.module('AppConfig');
  config.constant('Config',{
	  templates: "./apps/launch/templates/",
	  partials: './templates/partials/',
      appId:   '1861675950771308' //'551333468325789' //

  });

  config.factory('MainConfig',function($timeout, Config, BackendConfig) {

      "ngInject";

	  var backend;

	  var resolveConfig = function(cb) {
		  console.log('resolving..');
		  if(backend){
			  cb(true);
			  return;
		  }
		  BackendConfig.get(function(res) {
			  backend = res;
			  console.log(res);
    		  cb(true);
    	  }, function (err) {
    		  cb(false);
    	  });
	  };

	  var getBackend = function(){
		  return backend; //
	  };

	  var setBackend = function(config){
		 backend = config;
	  };


	  var basicScrollConfig = {
            autoHideScrollbar: false,
            theme: 'dark',
            axis: 'y',
            advanced:{
                updateOnContentResize: true
            },
            setHeight: '50vh',
            scrollInertia: 500,
            mouseWheel:{
                scrollAmount: 250
            }
      };

	  var getScrollConfig = function(callback){
		  var config = angular.copy(basicScrollConfig);
	      config.callbacks = {
	          onTotalScroll : callback
	      };
		  return config;
	  };

	  var mainmenu = [

      ];

	  var logoutmenu = [{
          name: 'Login',
          link: 'auth',
          icon: 'lock'
      }];

      var cleanResource = function(res){
          if(!res) return;
          delete res.$promise;
          delete res.$resolved;
          return res;

      };

	  return {
		  getScrollConfig: getScrollConfig,
		  basicScrollConfig : basicScrollConfig,
		  resolveConfig : resolveConfig,
		  getBackend : getBackend,
		  setBackend : setBackend,
		  mainmenu : mainmenu,
		  logoutmenu: logoutmenu,
          cleanResource : cleanResource
	  };


  });

})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('LaunchReadyController', function($scope,Player, Pagination, DetectBottom, $state, Claim, MainConfig) {

      "ngInject";

      $scope.launch_time = MainConfig.getBackend().launch_time;

      $scope.players = [];
      var paginator = Pagination.getPaginator(Player.search, {term:''}, function() {
          $scope.players = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      var detector = DetectBottom.getDetector(function(){
          paginator.loadMore();
      });

      $scope.$on('$destroy', function() {
        detector.off();
     });

     $scope.view = function(player){
         $state.go('players.view',{id:player.id});
     };

     var isUpdating = false;
     $scope.claim = function(player, handler){
         if(isUpdating) return;
         isUpdating = true;
         var model = {
             playerid : player.id
         };
         handler.update(Claim.create, model, function (res) {
             console.log(res);
             $scope.isDone = true;
             isUpdating = false;
         }, function (err) {
             isUpdating = false;
             console.log(err);
         });
     };



  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('NavController', function($scope, $rootScope, $timeout, $state, MainConfig) {

      "ngInject";

      $scope.isOpen = false; //!
      $scope.$state = $state;
      $scope.menu = MainConfig.logoutmenu;

      var openedViaMenuButton = false;


      $scope.$on('onLoginComplete',function(e,args) {
          if($rootScope.user){
            console.log($rootScope.user);
            $scope.menu = MainConfig.mainmenu;
          }
      });

      $scope.menu = MainConfig.mainmenu;

      $scope.$on('onLogoutComplete',function(e,args) {
          //$scope.menu = MainConfig.logoutmenu;
      });

      $scope.nav = function(item){
        $scope.isOpen = false;
        if(openedViaMenuButton){
            $timeout(function(){
                $state.go(item.link);
            },1000);
        }else{
            $state.go(item.link);
        }
      };

      $scope.openMenu = function(){
          openedViaMenuButton = true;
          $scope.isOpen = !$scope.isOpen;
      };

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('AuthController', function($rootScope, $timeout, $scope, $state, AuthService, LBHandler, $auth, MainConfig) {

      "ngInject";

      $scope.loading = true;

      if(AuthService.getIsLoggedOut()){
          $scope.loading = false;
      }

      $timeout(function () {
          console.log('timeout');
          $scope.loading = false;
      },10000); //wait for max 10 secs

      $scope.$on('onSdkLoad',function(){
          AuthService.getLoginStatus(function(data){

              if(!data.status){
                  console.log(data);
                  $scope.loading = false;
                  $scope.$apply();
              }
          });
      });

      $scope.$on('onLoginComplete',function(e,args) {
          console.log('Login Complete.');
          $state.go('launch');
      });


      $scope.login = function() {
          $scope.loading = true;
          $timeout(function () {
              console.log('timeout');
              $scope.loading = false;
          },10000);
          AuthService.login(function(res){

          });
      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('LogOutController', function($scope, AuthService, $state) {

      "ngInject";

      AuthService.logout(function() {
          $state.go('auth');
      });

  });
})();


(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('fbpageplugin', function(Config, $timeout){

            "ngInject";

            return {
                restrict: 'AEC',
                scope:{
                },
                link: function(scope, element, attr) {

                    $timeout(function () {
                        FB.XFBML.parse(element[0]);
                    },2000);
                },
                templateUrl: Config.templates+'directives/fbpageplugin.html'
            };
        });

})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('fbpic', function(Config){

            "ngInject";
            
            return {
                restrict: 'AEC',
                scope:{
                    fbid : '=',
                    dimension : '=',

                },
                link: function(scope, element, attr) {

                },
                templateUrl: Config.templates+'directives/fbpic.html'
            };
        });

})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('fadingbackgrounds', function(){

            "ngInject";

            return {
                link: function(scope, element, attrs, ngModel) {

                    var front = document.querySelector('.front');
                    var back = document.querySelector('.back');
                    var backUrls = ["url('./images/svg/messi.jpg')","url('./images/svg/suarez.jpg')", "url('./images/svg/hazard.jpg')", "url('./images/svg/gareth.jpg')", "url('./images/svg/aguero.jpg')"];
                    var frontUrls = ["url('./images/svg/cruyff.jpg')", "url('./images/svg/ronaldo.jpg')", "url('./images/svg/ibra.jpg')", "url('./images/svg/ozil.jpg')", "url('./images/svg/neymar.jpg')"];
                    var b = 0, f = 0;
                    var transition = function (isFadeIn) {
                        var opacity = (isFadeIn) ? 1 : 0;
                        dynamics.animate(front, {opacity: opacity}, {
                            type: dynamics.easeIn,
                            delay: 2000,
                            duration: 4000,
                            friction: 600,
                            complete: function () {
                                if (isFadeIn) {
                                    b = (b + 1) % backUrls.length;

                                    dynamics.css(back, {backgroundImage: backUrls[b]});
                                } else {
                                    f = (f + 1) % frontUrls.length;
                                    dynamics.css(front, {backgroundImage: frontUrls[f]});
                                }
                                transition(!isFadeIn);
                            }
                        });
                    };
                    transition(false);

                }
            };
        });

})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('lbwithhandler', function(Config, LBHandler){

            "ngInject";
            
            return {
                restrict: 'AEC',
                scope:{
                    player : '=',
                    fn : '=',
                    value : '@',
                },
                link: function(scope, element, attrs) {

                     scope.handler = LBHandler.getHandler(100);

                     scope.click = function(){
                         scope.fn(scope.player, scope.handler);
                     };

                     scope.value = attrs.value;

                     scope.$watch(function(scope){
                        return scope.handler.getState();
                     },function(newValue, oldValue){
                         scope.state = newValue;
                     });


                },
                templateUrl: Config.templates+'directives/lbwithhandler.html'
            };
        });

})();

(function()
{
    'use strict';

    angular
        .module('AppDirectives')
        .directive('loadingButton', function(Config){

            "ngInject";
            
            return {
                restrict: 'AEC',
                scope:{
                    handler: '=',
                    click : '=',
                    disableCriteria: '=',
                    lxType : '@',
                    lxSize : '@',
                    lxDiameter : '@',
                    lxColor: '@',
                    value : '@',
                    successText : '@',
                    failText : '@'
                },
                link: function(scope, element, attr) {
                    if(scope.handler){
                        scope.$watch(function(scope){
                            return scope.handler.getState();
                        },function(newValue, oldValue){
                            scope.state = newValue;
                        });
                    }
                },
                templateUrl: Config.templates+'directives/loadingbutton.html'
            };
        });

})();

(function()
{
    'use strict';

    angular
        .module('AppDirectives')
        .directive('appProgress', function lxProgress(Config){

            "ngInject";

            return {
                restrict: 'E',
                templateUrl: Config.templates+'directives/progress.html',
                scope:
                {
                    lxColor: '@?',
                    lxDiameter: '@?',
                    lxType: '@',
                    lxValue: '@'
                },
                controller: LxProgressController,
                controllerAs: 'lxProgress',
                bindToController: true,
                replace: true
            };
    });

    function LxProgressController()
    {
        var lxProgress = this;
        init();
        lxProgress.getCircularProgressValue = getCircularProgressValue;
        lxProgress.getLinearProgressValue = getLinearProgressValue;
        lxProgress.getProgressDiameter = getProgressDiameter;



        ////////////

        function getCircularProgressValue()
        {
            if (angular.isDefined(lxProgress.lxValue))
            {
                return {
                    'stroke-dasharray': lxProgress.lxValue * 1.26 + ',200'
                };
            }
        }

        function getLinearProgressValue()
        {
            if (angular.isDefined(lxProgress.lxValue))
            {
                return {
                    'transform': 'scale(' + lxProgress.lxValue / 100 + ', 1)'
                };
            }
        }

        function getProgressDiameter()
        {
            if (lxProgress.lxType === 'circular')
            {
                return {
                    'transform': 'scale(' + parseInt(lxProgress.lxDiameter) / 100 + ')'
                };
            }

            return;
        }

        function init()
        {
            lxProgress.lxDiameter = angular.isDefined(lxProgress.lxDiameter) ? lxProgress.lxDiameter : 100;
            lxProgress.lxColor = angular.isDefined(lxProgress.lxColor) ? lxProgress.lxColor : 'primary';
        }
    }
})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('stringToNumber', function(){

            "ngInject";
            
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, ngModel) {
                  ngModel.$parsers.push(function(value) {
                    return '' + value;
                  });
                  ngModel.$formatters.push(function(value) {
                    return parseFloat(value);
                  });
                }
            };
        });

})();


(function() {
  'use strict';
  var base = './api/claim'; //<---

  var services = angular.module('AppServices');


  services.factory('Claim', function($resource) {

      "ngInject";
      
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/config';

  var services = angular.module('AppServices');


  services.factory('BackendConfig', function($resource) {

      "ngInject";
      
    return $resource(base, {}, {
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/player'; //<---

  var services = angular.module('AppServices');


  services.factory('Player', function($resource) {

      "ngInject";

    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        groups: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/groups'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();


(function() {

	'use strict';
	var app = angular.module('MainRoutes', []);
	app.config(function($stateProvider, Config) {

        "ngInject";

        $stateProvider
	        .state('app', {
	            abstract: true,
	            url: '',
	            templateUrl: Config.templates + 'uiview.html'
	        })

			.state('game', {
	            abstract: true,
	            url: '',
				parent: 'app',
	            templateUrl: Config.templates + 'base.html',
	            controller: 'NavController'
	        })

			.state('auth', {
	            url: '/login',
	            parent: 'app',
	            templateUrl: Config.templates + 'login.html',
	            controller: 'AuthController'
	        })

            .state('launch', {
	            url: '/ready',
	            parent: 'app',
	            templateUrl: Config.templates + 'launch.html',
	            controller: 'LaunchReadyController'
	        })

			.state('logout', {
	            url: '/logout',
	            parent: 'app',
	            template: '',
	            controller: 'LogOutController'
	        });
  });

})();


(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('DetectBottom', function($window){

        "ngInject";

        var BottomDetect = function(callback){

            this.detect = function () {
                var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
                var body = document.body, html = document.documentElement;
                var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
                var windowBottom = windowHeight + window.pageYOffset;
                if (windowBottom >= (docHeight-25)) {
                    callback();
                }
            };

            this.on = function(cb){
                angular.element($window).on('scroll', this.detect);
            };

            this.off = function(){
                angular.element($window).off('scroll', this.detect);
            };

        };




        return {
            getDetector: function(cb){
                var d = new BottomDetect(cb);
                d.on();
                return d;
            }
        };

    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('LBHandler', function($timeout){

        "ngInject";
        
            var Handler = function(delay){
                var handler = this;
                handler.delay = delay;
                handler.state = 0; //0 - start, 1 - loading, 2 - success, 3 - failed
                handler.update = function(f,data,cb,errCb){
                    handler.state = 1;
                    f(data,function(res){
                        handler.state = 2;
                        handler.reset();
                        console.log(res);
                        cb(res);
                    },function(err){
                        handler.state = 3;
                        handler.reset();
                        console.log(err);
                        errCb(err);
                    });
                };

                handler.setState = function(state){
                    handler.state = state;
                };

                handler.reset = function(){
                        $timeout(function () {
                            handler.setState(0);
                        }, handler.delay);
                };

                handler.getState = function(){
                    return handler.state;
                };

            };


            return {
                getHandler: function(delay){
                    return new Handler(delay);
                }
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('Middleware', function($rootScope, $timeout, $state, MainConfig, $stateParams){

        "ngInject";

			var auth = function(isAuth, event, toState){

				if(isAuth){
					if(toState.name === 'auth') {
						event.preventDefault();
						$state.go('ready');
					}
				}else{
					if(toState.name !== 'auth'){
						event.preventDefault();
						$state.go('auth');
					}
				}
			};


            var fakeAuth = function(){
                $rootScope.authenticated = true;
                $rootScope.user = {"id":1,"name":"Awsin","email":"mail","liquidcash":"100000.00","points":0,"rank":0,"fbid":"15658"
                           };
                $timeout(function() {
                    console.log('Login');
                    $rootScope.$broadcast('onLoginComplete',{});
                },1500);
            };



            var fakeHelper = function(event,toState){

                if(!MainConfig.getBackend()){
    				event.preventDefault();
    				MainConfig.resolveConfig(function(success){
    					if(success)

    						$state.go(toState.name);

    				});
    			}else{
    				console.log(MainConfig.backend);
    			}
            };

            return {
				auth : auth,
                fakeHelper : fakeHelper,
                fakeAuth : fakeAuth
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('Pagination', function($timeout){

        "ngInject";

            var Paginator = function(pagefunc, params, success, fail){
                var paginator = this;

                paginator.reset = function(params){
                    paginator.page = 1;
                    paginator.results = [];
                    paginator.details = {};
                    paginator.params = params;
                };
                paginator.isLoading = false;
                paginator.reset(params);

                paginator.loadMore = function(){

                    if(paginator.isLoading){
                        return;
                    }

                    paginator.isLoading = true;
                    paginator.params.page = paginator.page;

                    pagefunc(paginator.params,function(response){
                        paginator.details = response;
                        console.log(paginator.page);
                        paginator.results = paginator.results.concat(response.data);
                        if(response.data.length>0){
                          paginator.page++;
                        }
                        paginator.isLoading = false;
                        console.log(response);
                        success(paginator.results, paginator.details);
                    },function(err){
                        console.log(err);
                        paginator.isLoading = false;
                        fail(err);
                    });
                };
            };
            return {
                getPaginator: function(pagefunc, success, fail){
                    return new Paginator(pagefunc, success, fail);
                }
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');

    services.factory('AuthFactory', function($resource) {

        "ngInject";

      return $resource('./api/', {}, {
          refreshToken: {
              method: 'POST',
              cache: false,
              isArray: false,
              url: 'api/refreshtoken'
          },
          password: {
              method: 'POST',
              cache: false,
              isArray: false,
              url: 'api/password'
          }
      });
    });


    services.factory('AuthService', function($rootScope,$auth,$state,$http, Config, MainConfig, $window){

        "ngInject";

    	var isLogggedOut = false;
        var explicitLogOut = false;

        var fbinit = function(){
            $window.fbAsyncInit = function() {

               FB.init({
                 appId: Config.appId,
                 status: true,
                 cookie: true,
                 xfbml: true,
                 version: 'v2.4'
                   });
                   watchLoginChange();
                   sdkLoaded();
            };

             (function(d){
               var js,
               id = 'facebook-jssdk',
               ref = d.getElementsByTagName('script')[0];
               if (d.getElementById(id)) {
                 return;
               }
               js = d.createElement('script');
               js.id = id;
               js.async = true;
               js.src = "//connect.facebook.net/en_US/all.js";
               ref.parentNode.insertBefore(js, ref);
             }(document));

        };

        var watchLoginChange = function() {
          FB.Event.subscribe('auth.authResponseChange', function(res) {
            if (res.status === 'connected') {
              console.log('connected');
    		  getUserInfo(function(res,token){

    			var credentials = {
    					 fb_token: token
    			 };

    			$auth
    			 .login(credentials)
    			 .then(function(response) {

    					isLogggedOut = false;
    					var data = response.data;
    					$auth.setToken(data.token);
    					$http.defaults.headers.common.Authorization = 'Bearer '+data.token;
    					$rootScope.authenticated = true;
    					$rootScope.user = data.user;
                        MainConfig.setBackend(data.config);

    					$rootScope.$broadcast('onLoginComplete',{});

    				 }, function(error) {
    					 console.log(error);
    					 $rootScope.authenticated = false;
    					 $rootScope.user = null;
    					 $auth.removeToken();
    			 });

    		  });

            }else {
              //not logged
              //console.log('not logged');

            }
          });
        };

    	var sdkLoaded = function(){
    		$rootScope.$broadcast('onSdkLoad',{});
    	};

    	var getLoginStatus = function(callback){
    		var data = {};
    		data.status = false;
    		FB.getLoginStatus(function(response) {
    		  if (response.status === 'connected') {
    		    	data.userid = response.authResponse.userID;
    		    	data.accessToken = response.authResponse.accessToken;
    				data.status = true;
    				callback(data);
    		  } else if (response.status === 'not_authorized') {
    			    callback(data);
    		  } else {
    			    callback(data);
    		  }
    		 });
    	};

        var getUserInfo = function(callback) {
          FB.api('/me', function(res) {
            var token =  FB.getAuthResponse().accessToken;
            $rootScope.$apply(function() {
              $rootScope.user = res;
              callback(res,token);
            });
          });
        };

    	var login = function(cb){
    		FB.login(function(response) {
    		    //let event system take care of login flow.
                console.log(response);
    		},{scope: 'email'});
    	};

        var logout = function(cb) {

          FB.logout(function(response) {
            $rootScope.$apply(function() {
    		  isLogggedOut = true;
              explicitLogOut = true;
    		  $http.defaults.headers.common.Authorization = '';
    		  $rootScope.authenticated = false;
      		  $rootScope.user = null;
      		  $auth.removeToken();
              $rootScope.$broadcast('onLogoutComplete',{});
    		  cb();
            });
          });
        };

    	var getIsLoggedOut = function(){
    		return isLogggedOut;
    	};

        var getExplicitLogOut = function(){
            return explicitLogOut;
        };


        return {
          watchLoginChange: watchLoginChange,
          getUserInfo: getUserInfo,
    	  getLoginStatus:getLoginStatus,
          logout: logout,
    	  login: login,
    	  sdkLoaded: sdkLoaded,
          fbinit: fbinit,
    	  getIsLoggedOut : getIsLoggedOut,
          getExplicitLogOut : getExplicitLogOut
        };
      });

})();
