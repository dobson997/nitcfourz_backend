
/*console.log = function(){

};*/

(function() {

	'use strict';

	var app = angular.module('app', [
			'ui.router',
			'satellizer',
			'AppControllers',
            'AppDirectives',
            'AppFilters',
			'AppServices',
			'AppConfig',
			'lumx',
			'AppRoutes'
	]);

	angular.module('AppControllers',[]);
	angular.module('AppDirectives',['lumx']);
	angular.module('AppServices',['ngResource']);
	angular.module('AppFilters',[]);
	angular.module('AppConfig',[]);
	angular.module('AppRoutes',[
		'MainRoutes',
	]);

	app.config(function($stateProvider, $urlRouterProvider, $authProvider, $httpProvider, $provide, Config) {

        "ngInject";

        function redirectWhenLoggedOut($q, $injector) {
            return {
                responseError: function(rejection) {

                    var $state = $injector.get('$state'); //can't inject state else circular dependency
                    var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];
                    angular.forEach(rejectionReasons, function(value, key) {
                        if(rejection.data.error === value) {
                            localStorage.removeItem('user');
                            $state.go('auth');
                        }
                    });
                    return $q.reject(rejection);
                }
            };
        }
        /* check */
        $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);
        $httpProvider.interceptors.push('redirectWhenLoggedOut');
        $authProvider.baseUrl = './';
        $authProvider.loginUrl = 'api/fbauthenticate';

        $urlRouterProvider.otherwise('/home');


	}).run(function($rootScope, $state, $auth, $window, $timeout, MainConfig, Middleware, AuthService) {

        "ngInject";

        AuthService.fbinit();

		$rootScope.user = {};
		$rootScope.go = function(state,params){
			$state.go(state,params);
		};

        $rootScope.$on('$stateChangeStart', function(event, toState) {

            console.log(toState);
            $("html, body").animate({scrollTop: 0}, "slow"); //not a good practice though!

            Middleware.auth($rootScope.authenticated,event,toState);
            //Middleware.fakeHelper(event, toState);

        });

        //Middleware.fakeAuth();

	});

})();

(function() {

	'use strict';

  var config = angular.module('AppConfig');
  config.constant('Config',{
	  templates: "./apps/frontend/templates/",
	  partials: './templates/partials/',
      appId:  '1861675950771308' // '551333468325789' // 

  });

  config.factory('MainConfig',function($timeout, Config, BackendConfig) {

      "ngInject";

	  var backend;

	  var resolveConfig = function(cb) {
		  console.log('resolving..');
		  if(backend){
			  cb(true);
			  return;
		  }
		  BackendConfig.get(function(res) {
			  backend = res;
			  console.log(res);
    		  cb(true);
    	  }, function (err) {
    		  cb(false);
    	  });
	  };

	  var getBackend = function(){
		  return backend; //
	  };

	  var setBackend = function(config){
		 backend = config;
	  };


	  var basicScrollConfig = {
            autoHideScrollbar: false,
            theme: 'dark',
            axis: 'y',
            advanced:{
                updateOnContentResize: true
            },
            setHeight: '50vh',
            scrollInertia: 500,
            mouseWheel:{
                scrollAmount: 250
            }
      };

	  var getScrollConfig = function(callback){
		  var config = angular.copy(basicScrollConfig);
	      config.callbacks = {
	          onTotalScroll : callback
	      };
		  return config;
	  };

	  var mainmenu = [
          {
              name: 'Home',
              link: 'home',
              parent : 'home',
              icon: 'home'
          },
		  {
              name: 'Game Rules',
              link: 'rules',
              parent : 'rules',
              icon: 'question'
          },
          {
              name: 'My Team',
              link: 'team.view',
              parent : 'team',
              icon: 'futbol-o'
          },
		  {
              name: 'Points',
              link: 'users.points',
              parent : 'users.points',
              icon: 'heart'
          },
		  {
              name: 'Transfer',
              link: 'transfer',
              parent : 'transfer',
              icon: 'exchange'
          },
		  {
			  name: 'Leaderboard',
			  link: 'users.leaderboard',
			  parent : 'users.leaderboard',
			  icon: 'trophy'
		  },
          {
			  name: 'Teams',
			  link: 'gameteams',
			  parent : 'gameteams',
			  icon: 'users'
		  },
		  {
			  name: 'Players',
			  link: 'players.all',
			  parent : 'players',
			  icon: 'user'
		  },
          {
			  name: 'Fixtures',
			  link: 'fixtures',
			  parent : 'fixtures',
			  icon: 'clock-o'
		  },
		  {
			  name: 'Facebook Feeds',
			  link: 'facebook',
			  parent : 'facebook',
			  icon: 'facebook'
		  },
          {
			  name: 'Logout',
			  link: 'logout',
			  parent : 'logout',
			  icon: 'sign-out'
		  }
      ];

	  var logoutmenu = [{
          name: 'Login',
          link: 'auth',
          icon: 'lock'
      }];

      var cleanResource = function(res){
          if(!res) return;
          delete res.$promise;
          delete res.$resolved;
          return res;

      };

	  return {
		  getScrollConfig: getScrollConfig,
		  basicScrollConfig : basicScrollConfig,
		  resolveConfig : resolveConfig,
		  getBackend : getBackend,
		  setBackend : setBackend,
		  mainmenu : mainmenu,
		  logoutmenu: logoutmenu,
          cleanResource : cleanResource
	  };


  });

})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('NavController', function($scope, $rootScope, $timeout, $state, MainConfig) {

      "ngInject";

      $scope.isOpen = false; //!
      $scope.$state = $state;
      $scope.menu = MainConfig.logoutmenu;

      var openedViaMenuButton = false;


      $scope.$on('onLoginComplete',function(e,args) {
          if($rootScope.user){
            console.log($rootScope.user);
            $scope.menu = MainConfig.mainmenu;
          }
      });

      $scope.menu = MainConfig.mainmenu;

      $scope.$on('onLogoutComplete',function(e,args) {
          //$scope.menu = MainConfig.logoutmenu;
      });

      $scope.nav = function(item){
        $scope.isOpen = false;
        if(openedViaMenuButton){
            $timeout(function(){
                $state.go(item.link);
            },1000);
        }else{
            $state.go(item.link);
        }
      };

      $scope.openMenu = function(){
          openedViaMenuButton = true;
          $scope.isOpen = !$scope.isOpen;
      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('RulesController', function($scope, MainConfig) {

      "ngInject";

      $scope.rules = MainConfig.getBackend().rules;

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('TeamsController', function($scope, Team, Share) {

      "ngInject";

      Team.getAll(function (res) {
          $scope.teams = res;
      }, function (err) {
         console.log(err);
      });

      $scope.input = {};

      $scope.changeTeam = function () {
          console.log($scope.input.selectTeam);
        if(!$scope.input.selectTeam)  return;
        Team.get({id:$scope.input.selectTeam.id},function (res) {
            console.log(res);
            $scope.currentTeam = res;
        },function (err) {
            console.log(err);
        });
      };

      $scope.share = function(){
          if(!$scope.input.selectTeam) return;
          var id = $scope.input.selectTeam.id;
          FB.ui({
           method: 'share',
           href: 'http://fourznitc.in/share/team/'+id
           }, function(response){});
      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('TransferController', function($scope, Player, Playerhistory, MainConfig, $rootScope, LBHandler) {

      "ngInject";

      $scope.input = {};

      $scope.deadline = MainConfig.getBackend().transfer_deadline;
      $scope.transfer_deadline_moment = moment($scope.deadline);
      var now = moment();
      $scope.isOpen = now.isBefore($scope.transfer_deadline_moment);

      console.log(now.format());
      console.log($scope.deadline);
      console.log($scope.transfer_deadline_moment.format());

      $scope.playerTypes = MainConfig.getBackend().playerTypes;
      if($rootScope.user.team){
          var team = $rootScope.user.team;
          $scope.currentTeam = [
              team.goalkeeper,
              team.player1,
              team.player2,
              team.player3,
              team.sub
          ];
      }

      $scope.handler = LBHandler.getHandler(1000);


      Player.groups(function (res) {
          console.log(res);
          delete res.$promise;
          delete res.$resolved;

          $scope.teamOptions = Object.keys(res);
          $scope.teams = res;
      }, function (err) {
         console.log(err);
      });

      $scope.changeTeam = function() {
          if($scope.input.selectTeam){
              $scope.team = $scope.teams[$scope.input.selectTeam];
          }
      };

      $scope.changePlayer = function(){
          if($scope.input.selectPlayer){
              $scope.current = $scope.input.selectPlayer;
              console.log($scope.current.teamname);
          }
      };
      // returns true if player belongs
      $scope.is_taken = function(player){
          for(var i=0;i<$scope.currentTeam.length;i++){
              console.log($scope.currentTeam[i].teamname+' '+$scope.input.selectPlayer.teamname);
              if($scope.currentTeam[i].teamname==player.teamname&&$scope.currentTeam[i].teamname!=$scope.input.selectPlayer.teamname){
                  return true;
              }
          }
          return false;
      };

      $scope.is_disabled = function(player){
        console.log(player.type+' '+$scope.input.selectPlayer.type);
        if(player.type!=$scope.input.selectPlayer.type)
            return true;
        if(player.price>(1*$scope.user.liquidcash+1*$scope.input.selectPlayer.price))
            return true;
        return false;
      };

      var isLoading = false;

      $scope.exchange = function (player, handler) {
        if(isLoading) return;
        isLoading = true;
        if($scope.is_disabled(player) || $scope.is_taken(player)) return;

        var model = {
            current : $scope.input.selectPlayer.id,
            newplayer : player.id
        };
        handler.update(Playerhistory.exchange,model,function (res) {
            console.log(res);
            $rootScope.user = res;
            isLoading = false;
            $scope.go('team.view');
        },function (err) {
            console.log(err);
            isLoading = false;
        });
      };


  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('AuthController', function($rootScope, $timeout, $scope, $state, AuthService, LBHandler, $auth, MainConfig) {

      $scope.loading = true;

      if(AuthService.getIsLoggedOut()){
          $scope.loading = false;
      }

      $timeout(function () {
          console.log('timeout');
          $scope.loading = false;
      },10000); //wait for max 10 secs

      $scope.$on('onSdkLoad',function(){
          AuthService.getLoginStatus(function(data){

              if(!data.status){
                  console.log(data);
                  $scope.loading = false;
                  $scope.$apply();
              }
          });
      });

      $scope.$on('onLoginComplete',function(e,args) {
          console.log('Login Complete.');
          $state.go('home');
      });


      $scope.login = function() {
          $scope.loading = true;
          $timeout(function () {
              console.log('timeout');
              $scope.loading = false;
          },10000);
          AuthService.login(function(res){

          });
      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('LogOutController', function($scope, AuthService, $state) {

      "ngInject";

      AuthService.logout(function() {
          $state.go('auth');
      });

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('FacebookFeedController', function($scope) {

      "ngInject";

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('FixtureController', function($scope, Fixture) {

      "ngInject";

      $scope.input = {};

      Fixture.days(function(res){
          console.log(res);
          $scope.days = res;
          if(res.length>0){
              $scope.input.selectDate = res[0];
              $scope.getFixture();
          }
      },function (err) {
         console.log(err);
      });



      $scope.getFixture = function () {
          var date = $scope.input.selectDate.date;
          if(!date) return;
          Fixture.fixture({date:date}, function(res){
              $scope.current = res;
              console.log(res);
          }, function (err) {
             console.log(err);
          });
      };

      $scope.toggle = function (f) {
        if(f.goals1===null) return;
        f.show = !f.show ;
      };



  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('MyTeamController', function($scope, $rootScope, MainConfig, Playerhistory, LBHandler, Share, $timeout) {

      "ngInject";

      if(!$rootScope.user.team){
          console.log($rootScope.user.team);
          $scope.go('team.create',{}, {location:'replace'});
      }

      $scope.deadline = MainConfig.getBackend().deadline_gw;
      $scope.deadline_moment = moment($scope.deadline);
      $scope.isOpen = moment().isBefore($scope.deadline_moment);



      var setData = function(){
          $scope.team = $rootScope.user.team;
          var team = $rootScope.user.team;
           if(!team) return;
          $scope.manager = [
              team.player1,
              team.player2,
              team.player3,
          ];
          $scope.goalkeeper =team.goalkeeper;
          $scope.sub = team.sub;
      };
      setData();

      var isUpdating = false;
      $scope.shuffle = function(player, handler){
          if(isUpdating) return;
          isUpdating = true;
          var model = {
              currentsub : $scope.sub.id,
              newsub : player.id
          };
          console.log(model);
          handler.update(Playerhistory.shuffle, model, function (res) {
              console.log(res);
              $rootScope.user = res;
              setData();
              isUpdating = false;
          }, function (err) {
              isUpdating = false;
             console.log(err);
          });
      };

      $scope.shareHandler = LBHandler.getHandler(0);

      $scope.share = function(){

          $scope.shareHandler.update(Share.create, function (res) {
              console.log(res);
              var id = res.id;

              $timeout(function () {
                $scope.shareHandler.setState(0);
                shareTeam(id);
            }, 5000);

          }, function (err) {
              console.log(err);
          });

      };

      var shareTeam = function(id){
          FB.ui({
           method: 'share',
           href: 'http://fourznitc.in/share/myteam/'+id
           }, function(response){});
      };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('MyTeamCreateController', function( $rootScope, $scope, Player, MainConfig, LBHandler, Playerhistory) {

      "ngInject";

      $scope.playerTypes = MainConfig.getBackend().playerTypes;
      var GOAL_KEEPER = 2;
      var GOAL_KEEPER_LIMIT = 1;
      var PLAYER = 1;
      var PLAYER_LIMIT = 4;


      Player.groups(function (res) {
          console.log(res);
          delete res.$promise;
          delete res.$resolved;

          $scope.teamOptions = Object.keys(res);
          $scope.teams = res;
      }, function (err) {
         console.log(err);
      });

      $scope.changeTeam = function() {
          if($scope.input.selectTeam){
              $scope.team = $scope.teams[$scope.input.selectTeam];
          }
      };

      $scope.added = [];
      $scope.total = 0;
      $scope.goalkeeper_count = 0;
      $scope.player_count = 0;

      $scope.add = function(player){
          //ensure he can be added!!
          if($scope.is_disabled(player) || $scope.is_taken(player)) return;

          $scope.added.push(player);
          $scope.total += 1*player.price;
          if(player.type==GOAL_KEEPER){
              $scope.goalkeeper_count++;
          }else{
              $scope.player_count++;
          }
      };

      $scope.remove = function(index){
          var p = $scope.added[index];
          if(p.type==GOAL_KEEPER){
              $scope.goalkeeper_count--;
          }else{
              $scope.player_count--;
          }
          $scope.total -= p.price;
          $scope.added.splice(index,1);
      };

      $scope.is_taken = function(player){
          for(var i=0;i<$scope.added.length;i++){
              if($scope.added[i].teamname==player.teamname){
                  return true;
              }
          }
          return false;
      };

      $scope.is_disabled = function(player){
        if(player.type==GOAL_KEEPER && $scope.goalkeeper_count==GOAL_KEEPER_LIMIT)
            return true;
        if(player.type==PLAYER && $scope.player_count==PLAYER_LIMIT)
            return true;
        if(player.price>($scope.user.liquidcash-$scope.total))
            return true;
        return false;
      };

      $scope.createCriteria = function() {
        var criteria = $scope.goalkeeper_count==GOAL_KEEPER_LIMIT && $scope.player_count==PLAYER_LIMIT;
        return   criteria;
      };

      $scope.handler = LBHandler.getHandler(1000);

      $scope.createTeam = function(){
          console.log('creating..');
          var model = {};
          var players = angular.copy($scope.added);
          console.log($scope.added);
          for (var i = 0; i < players.length; i++) {
             if(players[i].type == GOAL_KEEPER){
                model.goalkeeper = players[i].id;
                players.splice(i,1);
                break;
             }
          }
          model.player1 = players[0].id;
          model.player2 = players[1].id;
          model.player3 = players[2].id;
          model.sub = players[3].id;
          console.log(model);
          $scope.handler.update(Playerhistory.create, model, function(res){
             console.log(res);
             $rootScope.user = res;
             $scope.go('team.view');
         },function (err) {
            console.log(err);
          });

      };



  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('PlayerController', function($scope, Player, Pagination, DetectBottom, $state) {

      "ngInject";

      $scope.players = [];
      var paginator = Pagination.getPaginator(Player.search, {term:''}, function() {
          $scope.players = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();

      $scope.input = {};

      $scope.search = function(){
          $scope.term = $scope.input.searchterm;
          paginator.reset({term:$scope.term});
          paginator.loadMore();
      };

      var detector = DetectBottom.getDetector(function(){
          paginator.loadMore();
      });

      $scope.$on('$destroy', function() {
        detector.off();
     });

     $scope.view = function(player){
         $state.go('players.view',{id:player.id});
     };

  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('PlayerViewController', function($scope, $stateParams, Player, Gameweek) {

      "ngInject";

      var id = $stateParams.id;
      Player.get({id:id}, function(res){
          console.log(res);
          $scope.player = res;
      }, function(err){
          console.log(err);
      });

      Gameweek.player({id:id}, function(res){
          console.log(res);
          $scope.details = res;
      }, function(err) {
          console.log(err);
      });

  });
})();


(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('LeaderboardController', function($scope) {

      "ngInject";

     $scope.detector.on();


  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');
  controllers.controller('PointsController', function($rootScope, $scope, Playerhistory, Gameweek, $stateParams) {

      "ngInject";

      $scope.detector.off();


      if(!$scope.toshow){
          $scope.isYou = true;
          $scope.toshow = angular.copy($rootScope.user); //not to let it pollute rootscope user!
      }

    $scope.input = {};

    Gameweek.weeks(function (res) {
        $scope.weeks = res;
        if(res.length>0){
            $scope.input.selectGameweek = res[0];
            $scope.changeGameWeek();
        }
        console.log(res);
    },function (err) {
        console.log(err);
    });

    $scope.changeGameWeek = function(){
        $scope.isError = false;
        $scope.isDone = false;
        var weekid = $scope.input.selectGameweek.gameweek;
        Playerhistory.weekpoints({userid:$scope.toshow.id,weekid:weekid}, function (res) {
            $scope.isDone = true;
            console.log(res);
            $scope.detail = res;
            $scope.game = res.game;
            $scope.player = res.player;
            $scope.getTotal($scope.game,$scope.player);

        }, function (err) {
            $scope.isError = true;
            $scope.isDone = true;
           console.log(err);
        });

    };

    $scope.getTotal = function (game, player) {
        // var total = game[player.player1].total +
        //     game[player.player2].total +
        //     game[player.player3].total +
        //     game[player.goalkeeper].total;

        if(game[player.player1].hasplayed=='1'&&game[player.player2].hasplayed=='1'&&game[player.player3].hasplayed=='1'){

            console.log('case 1. all played');

            //do nothing huh?

        }else if(game[player.sub].hasplayed=='1')

            console.log('case 2. Sub played & someone didnt');

            if(game[player.player1].hasplayed=='0'){
                //total += game[player.sub].total;
                flip(player,"sub","player1");
            }else if(game[player.player2].hasplayed=='0'){
                //total += game[player.sub].total;
                flip(player,"sub","player2");
            }else if(game[player.player3].hasplayed=='0'){
                //total += game[player.sub].total;
                flip(player,"sub","player3");
            }

            // console.log(total);
            // $scope.total = total;
        };


    var flip = function(player, p1, p2){

        console.log('flipping'+p1+' and '+p2);
        var l = player[p1];
        player[p1] = player[p2];
        player[p2] = l;

    };





  });
})();

(function() {
  'use strict';

  var controllers = angular.module('AppControllers');

  controllers.controller('UsersController', function($scope, User, Pagination, DetectBottom) {

      "ngInject";

      $scope.users = [];
      var paginator = Pagination.getPaginator(User.getAll, {}, function() {
          $scope.users = paginator.results;
          $scope.details = paginator.details;
      }, function (err) {
          $scope.errors = err.data.errors;
      });

      paginator.loadMore();

      $scope.detector = DetectBottom.getDetector(function(){
          paginator.loadMore();
      });


      $scope.detector.off();

      $scope.$on('$destroy', function() {
        $scope.detector.off();
     });

     $scope.showUser = function (user) {
        $scope.toshow = user;
        $scope.go('users.points');
     };


  });
})();


(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('fbpageplugin', function(Config, $timeout){

            "ngInject";
            
            return {
                restrict: 'AEC',
                scope:{
                },
                link: function(scope, element, attr) {

                    $timeout(function () {
                        FB.XFBML.parse(element[0]);
                    },2000);
                },
                templateUrl: Config.templates+'directives/fbpageplugin.html'
            };
        });

})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('fbpic', function(Config){

            "ngInject";
            
            return {
                restrict: 'AEC',
                scope:{
                    fbid : '=',
                    dimension : '=',

                },
                link: function(scope, element, attr) {

                },
                templateUrl: Config.templates+'directives/fbpic.html'
            };
        });

})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('fadingbackgrounds', function(){

            "ngInject";

            return {
                link: function(scope, element, attrs, ngModel) {

                    var front = document.querySelector('.front');
                    var back = document.querySelector('.back');
                    var backUrls = ["url('./images/svg/messi.jpg')","url('./images/svg/suarez.jpg')", "url('./images/svg/hazard.jpg')", "url('./images/svg/gareth.jpg')", "url('./images/svg/aguero.jpg')"];
                    var frontUrls = ["url('./images/svg/cruyff.jpg')", "url('./images/svg/ronaldo.jpg')", "url('./images/svg/ibra.jpg')", "url('./images/svg/ozil.jpg')", "url('./images/svg/neymar.jpg')"];
                    var b = 0, f = 0;
                    var transition = function (isFadeIn) {
                        var opacity = (isFadeIn) ? 1 : 0;
                        dynamics.animate(front, {opacity: opacity}, {
                            type: dynamics.easeIn,
                            delay: 2000,
                            duration: 4000,
                            friction: 600,
                            complete: function () {
                                if (isFadeIn) {
                                    b = (b + 1) % backUrls.length;

                                    dynamics.css(back, {backgroundImage: backUrls[b]});
                                } else {
                                    f = (f + 1) % frontUrls.length;
                                    dynamics.css(front, {backgroundImage: frontUrls[f]});
                                }
                                transition(!isFadeIn);
                            }
                        });
                    };
                    transition(false);

                }
            };
        });

})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('lbwithhandler', function(Config, LBHandler){

            "ngInject";
            
            return {
                restrict: 'AEC',
                scope:{
                    player : '=',
                    fn : '=',
                    value : '@',
                },
                link: function(scope, element, attrs) {

                     scope.handler = LBHandler.getHandler(100);

                     scope.click = function(){
                         scope.fn(scope.player, scope.handler);
                     };

                     scope.value = attrs.value;

                     scope.$watch(function(scope){
                        return scope.handler.getState();
                     },function(newValue, oldValue){
                         scope.state = newValue;
                     });


                },
                templateUrl: Config.templates+'directives/lbwithhandler.html'
            };
        });

})();

(function()
{
    'use strict';

    angular
        .module('AppDirectives')
        .directive('loadingButton', function(Config){

            "ngInject";
            
            return {
                restrict: 'AEC',
                scope:{
                    handler: '=',
                    click : '=',
                    disableCriteria: '=',
                    lxType : '@',
                    lxSize : '@',
                    lxDiameter : '@',
                    lxColor: '@',
                    value : '@',
                    successText : '@',
                    failText : '@'
                },
                link: function(scope, element, attr) {
                    if(scope.handler){
                        scope.$watch(function(scope){
                            return scope.handler.getState();
                        },function(newValue, oldValue){
                            scope.state = newValue;
                        });
                    }
                },
                templateUrl: Config.templates+'directives/loadingbutton.html'
            };
        });

})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('pitchdiv', function(Config){
            
            "ngInject";

            return {
                restrict: 'AEC',
                scope:{
                    game : '=',
                    player : '='
                },
                link: function(scope, element, attr) {

                },
                templateUrl: Config.templates+'directives/pitch.html'
            };
        });

})();

(function()
{
    'use strict';

    angular
        .module('AppDirectives')
        .directive('appProgress', function lxProgress(Config){

            "ngInject";

            return {
                restrict: 'E',
                templateUrl: Config.templates+'directives/progress.html',
                scope:
                {
                    lxColor: '@?',
                    lxDiameter: '@?',
                    lxType: '@',
                    lxValue: '@'
                },
                controller: LxProgressController,
                controllerAs: 'lxProgress',
                bindToController: true,
                replace: true
            };
    });

    function LxProgressController()
    {
        var lxProgress = this;
        init();
        lxProgress.getCircularProgressValue = getCircularProgressValue;
        lxProgress.getLinearProgressValue = getLinearProgressValue;
        lxProgress.getProgressDiameter = getProgressDiameter;



        ////////////

        function getCircularProgressValue()
        {
            if (angular.isDefined(lxProgress.lxValue))
            {
                return {
                    'stroke-dasharray': lxProgress.lxValue * 1.26 + ',200'
                };
            }
        }

        function getLinearProgressValue()
        {
            if (angular.isDefined(lxProgress.lxValue))
            {
                return {
                    'transform': 'scale(' + lxProgress.lxValue / 100 + ', 1)'
                };
            }
        }

        function getProgressDiameter()
        {
            if (lxProgress.lxType === 'circular')
            {
                return {
                    'transform': 'scale(' + parseInt(lxProgress.lxDiameter) / 100 + ')'
                };
            }

            return;
        }

        function init()
        {
            lxProgress.lxDiameter = angular.isDefined(lxProgress.lxDiameter) ? lxProgress.lxDiameter : 100;
            lxProgress.lxColor = angular.isDefined(lxProgress.lxColor) ? lxProgress.lxColor : 'primary';
        }
    }
})();

(function(){
    'use strict';
    angular.module('AppDirectives')
        .directive('stringToNumber', function(){

            "ngInject";
            
            return {
                require: 'ngModel',
                link: function(scope, element, attrs, ngModel) {
                  ngModel.$parsers.push(function(value) {
                    return '' + value;
                  });
                  ngModel.$formatters.push(function(value) {
                    return parseFloat(value);
                  });
                }
            };
        });

})();


(function(){
    'use strict';
    angular.module('AppFilters')
       .filter('playertype',function(MainConfig){

           "ngInject";

            return function(input){
                var v = MainConfig.getBackend().playerTypes[input];
                if(v){
                    return v;
                }else{
                    return 'Wrong Type!';
                }
            };

        });
})();


(function() {
  'use strict';
  var base = './api/config';

  var services = angular.module('AppServices');


  services.factory('BackendConfig', function($resource) {

      "ngInject";

    return $resource(base, {}, {
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/fixtures'; //<---

  var services = angular.module('AppServices');


  services.factory('Fixture', function($resource) {

      "ngInject";
      
    return $resource('./api/', {id:'@id', date: '@date'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        days : {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/days'
        },
        fixture : {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/fixture/:date'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/gameweek'; //<---

  var services = angular.module('AppServices');


  services.factory('Gameweek', function($resource) {

      "ngInject";
      
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        player : {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/player/:id'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        weeks: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/weeks'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/player'; //<---

  var services = angular.module('AppServices');


  services.factory('Player', function($resource) {

      "ngInject";
      
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        groups: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/groups'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/playerhistory'; //<---

  var services = angular.module('AppServices');


  services.factory('Playerhistory', function($resource) {

      "ngInject";
      
    return $resource('./api/', {id:'@id', userid:'@userid', weekid: '@weekid'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        weekpoints: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/weekpoints/:userid/:weekid'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: './api/temporary/create' //<-------
        },
        exchange : {
            method: 'POST',
            cache: false,
            isArray: false,
            url: './api/temporary/exchange' //<-------
        },
        shuffle: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: './api/temporary/shuffle' //<-------
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/shares'; //<---

  var services = angular.module('AppServices');


  services.factory('Share', function($resource) {

      "ngInject";
      
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/team'; //<---

  var services = angular.module('AppServices');


  services.factory('Team', function($resource) {

      "ngInject";
      
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: true,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();

(function() {
  'use strict';
  var base = './api/user'; //<---

  var services = angular.module('AppServices');


  services.factory('User', function($resource) {

      "ngInject";
      
    return $resource('./api/', {id:'@id'}, {
        getAll: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/all'
        },
        get: {
            method: 'GET',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        create: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/create'
        },
        post: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id'
        },
        delete: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/:id/delete'
        },
        search: {
            method: 'POST',
            cache: false,
            isArray: false,
            url: base + '/search'
        }
    });
  });
})();


(function() {

	'use strict';
	var app = angular.module('MainRoutes', []);
	app.config(function($stateProvider, Config) {

        "ngInject";

        $stateProvider
	        .state('app', {
	            abstract: true,
	            url: '',
	            templateUrl: Config.templates + 'uiview.html'
	        })

			.state('game', {
	            abstract: true,
	            url: '',
				parent: 'app',
	            templateUrl: Config.templates + 'base.html',
	            controller: 'NavController'
	        });

        $stateProvider
            .state('home', {
                url: '/home',
                parent : 'game',
                templateUrl: Config.templates + 'home.html',
            })

			.state('team', {
                url: '/team',
                parent : 'game',
				abstract: true,
                templateUrl: Config.templates + 'uiview.html'
            })

            .state('team.view', {
                url: '/view',
                templateUrl: Config.templates + 'myteam/view.html',
				controller : 'MyTeamController'
            })

			.state('team.create', {
                url: '/create',
                templateUrl: Config.templates + 'myteam/create.html',
				controller : 'MyTeamCreateController'
            })

            //leaderboard and teams view share users

            .state('users' , {
                url: '/u',
                parent : 'game',
                abstract : true,
                templateUrl: Config.templates + 'uiview.html',
				controller: 'UsersController'
            })

			.state('users.leaderboard', {
                url: '/leaderboard',
                templateUrl: Config.templates + 'users/leaderboard.html',
				controller: 'LeaderboardController'
            })


            .state('users.points', {
                url: '/points',
                templateUrl: Config.templates + 'users/points.html',
				controller: 'PointsController'
            })





			.state('players', {
                url: '/players',
				parent : 'game',
				abstract: true,
                templateUrl: Config.templates + 'uiview.html'
            })

			.state('players.all', {
                url: '/all',
                templateUrl: Config.templates + 'player/players.html',
				controller : 'PlayerController'
            })

			.state('players.view', {
                url: '/view/:id',
                templateUrl: Config.templates + 'player/view.html',
				controller : 'PlayerViewController'
            })

			.state('transfer', {
                url: '/transfer',
                parent : 'game',
                templateUrl: Config.templates + 'transfer/transfer.html',
				controller: 'TransferController'
            })

			.state('facebook', {
                url: '/facebook',
                parent : 'game',
                templateUrl: Config.templates + 'facebook/facebook.html',
				controller: 'FacebookFeedController'
            })



			.state('rules', {
                url: '/rules',
                parent : 'game',
                templateUrl: Config.templates + 'rules/rules.html',
                controller: 'RulesController'
            })

            .state('fixtures', {
                url: '/fixtures',
                parent : 'game',
                templateUrl: Config.templates + 'fixtures/fixtures.html',
                controller : 'FixtureController'
            })

            .state('gameteams', {
                url: '/gameteams',
                parent : 'game',
                templateUrl: Config.templates + 'teams/teams.html',
                controller : 'TeamsController'
            })




			.state('auth', {
	            url: '/login',
	            parent: 'app',
	            templateUrl: Config.templates + 'login.html',
	            controller: 'AuthController'
	        })

			.state('logout', {
	            url: '/logout',
	            parent: 'app',
	            template: '',
	            controller: 'LogOutController'
	        });
  });

})();


(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('DetectBottom', function($window){

        "ngInject";

        var BottomDetect = function(callback){

            this.detect = function () {
                var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
                var body = document.body, html = document.documentElement;
                var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
                var windowBottom = windowHeight + window.pageYOffset;
                if (windowBottom >= (docHeight-25)) {
                    callback();
                }
            };

            this.on = function(cb){
                angular.element($window).on('scroll', this.detect);
            };

            this.off = function(){
                angular.element($window).off('scroll', this.detect);
            };

        };




        return {
            getDetector: function(cb){
                var d = new BottomDetect(cb);
                d.on();
                return d;
            }
        };

    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('LBHandler', function($timeout){

        "ngInject";
        
            var Handler = function(delay){
                var handler = this;
                handler.delay = delay;
                handler.state = 0; //0 - start, 1 - loading, 2 - success, 3 - failed
                handler.update = function(f,data,cb,errCb){
                    handler.state = 1;
                    f(data,function(res){
                        handler.state = 2;
                        handler.reset();
                        console.log(res);
                        cb(res);
                    },function(err){
                        handler.state = 3;
                        handler.reset();
                        console.log(err);
                        errCb(err);
                    });
                };

                handler.setState = function(state){
                    handler.state = state;
                };

                handler.reset = function(){
                        $timeout(function () {
                            handler.setState(0);
                        }, handler.delay);
                };

                handler.getState = function(){
                    return handler.state;
                };

            };


            return {
                getHandler: function(delay){
                    return new Handler(delay);
                }
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('Middleware', function($rootScope, $timeout, $state, MainConfig, $stateParams){

        "ngInject";

			var auth = function(isAuth, event, toState){

				if(isAuth){
					if(toState.name === 'auth') {
						event.preventDefault();
						$state.go('home');
					}
				}else{
					if(toState.name !== 'auth'){
						event.preventDefault();
						$state.go('auth');
					}
				}
			};


            var fakeAuth = function(){
                $rootScope.authenticated = true;
                $rootScope.user = {"id":1,"name":"Awsin","email":"mail","liquidcash":"100000.00","points":0,"rank":0,"fbid":"15658",
                            "team":
                                {"id":1,"userid":1,
                                "goalkeeper":{"id":1,"name":"Sanath","price":"500.00","type":1,"teamname":"JFC","fbid":"4"},
                                "player1":{"id":1,"name":"Sanath","price":"500.00","type":1,"teamname":"JFC","fbid":"4"},
                                "player2":{"id":1,"name":"Sanath","price":"500.00","type":1,"teamname":"JFC","fbid":"4"},
                                "player3":{"id":1,"name":"Sanath","price":"500.00","type":1,"teamname":"JFC","fbid":"4"},
                                "sub":{"id":1,"name":"Sanath","price":"500.00","type":1,"teamname":"JFC","fbid":"4"}
                               }
                           };
                $timeout(function() {
                    console.log('Login');
                    $rootScope.$broadcast('onLoginComplete',{});
                },1500);
            };



            var fakeHelper = function(event,toState){

                if(!MainConfig.getBackend()){
    				event.preventDefault();
    				MainConfig.resolveConfig(function(success){
    					if(success)

    						$state.go(toState.name);

    				});
    			}else{
    				console.log(MainConfig.backend);
    			}
            };

            return {
				auth : auth,
                fakeHelper : fakeHelper,
                fakeAuth : fakeAuth
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');
	services.factory('Pagination', function($timeout){

        "ngInject";

            var Paginator = function(pagefunc, params, success, fail){
                var paginator = this;

                paginator.reset = function(params){
                    paginator.page = 1;
                    paginator.results = [];
                    paginator.details = {};
                    paginator.params = params;
                };
                paginator.isLoading = false;
                paginator.reset(params);

                paginator.loadMore = function(){

                    if(paginator.isLoading){
                        return;
                    }

                    paginator.isLoading = true;
                    paginator.params.page = paginator.page;

                    pagefunc(paginator.params,function(response){
                        paginator.details = response;
                        console.log(paginator.page);
                        paginator.results = paginator.results.concat(response.data);
                        if(response.data.length>0){
                          paginator.page++;
                        }
                        paginator.isLoading = false;
                        console.log(response);
                        success(paginator.results, paginator.details);
                    },function(err){
                        console.log(err);
                        paginator.isLoading = false;
                        fail(err);
                    });
                };
            };
            return {
                getPaginator: function(pagefunc, success, fail){
                    return new Paginator(pagefunc, success, fail);
                }
            };
    });
})();

(function() {
    'use strict';
    var services = angular.module('AppServices');

    services.factory('AuthFactory', function($resource) {

        "ngInject";

      return $resource('./api/', {}, {
          refreshToken: {
              method: 'POST',
              cache: false,
              isArray: false,
              url: 'api/refreshtoken'
          },
          password: {
              method: 'POST',
              cache: false,
              isArray: false,
              url: 'api/password'
          }
      });
    });


    services.factory('AuthService', function($rootScope,$auth,$state,$http, Config, MainConfig, $window){

        "ngInject";

    	var isLogggedOut = false;
        var explicitLogOut = false;

        var fbinit = function(){
            $window.fbAsyncInit = function() {

               FB.init({
                 appId: Config.appId,
                 status: true,
                 cookie: true,
                 xfbml: true,
                 version: 'v2.4'
                   });
                   watchLoginChange();
                   sdkLoaded();
            };

             (function(d){
               var js,
               id = 'facebook-jssdk',
               ref = d.getElementsByTagName('script')[0];
               if (d.getElementById(id)) {
                 return;
               }
               js = d.createElement('script');
               js.id = id;
               js.async = true;
               js.src = "//connect.facebook.net/en_US/all.js";
               ref.parentNode.insertBefore(js, ref);
             }(document));

        };

        var watchLoginChange = function() {
          FB.Event.subscribe('auth.authResponseChange', function(res) {
            if (res.status === 'connected') {
              console.log('connected');
    		  getUserInfo(function(res,token){

    			var credentials = {
    					 fb_token: token
    			 };

    			$auth
    			 .login(credentials)
    			 .then(function(response) {

    					isLogggedOut = false;
    					var data = response.data;
    					$auth.setToken(data.token);
    					$http.defaults.headers.common.Authorization = 'Bearer '+data.token;
    					$rootScope.authenticated = true;
    					$rootScope.user = data.user;
                        MainConfig.setBackend(data.config);

    					$rootScope.$broadcast('onLoginComplete',{});

    				 }, function(error) {
    					 console.log(error);
    					 $rootScope.authenticated = false;
    					 $rootScope.user = null;
    					 $auth.removeToken();
    			 });

    		  });

            }else {
              //not logged
              //console.log('not logged');

            }
          });
        };

    	var sdkLoaded = function(){
    		$rootScope.$broadcast('onSdkLoad',{});
    	};

    	var getLoginStatus = function(callback){
    		var data = {};
    		data.status = false;
    		FB.getLoginStatus(function(response) {
    		  if (response.status === 'connected') {
    		    	data.userid = response.authResponse.userID;
    		    	data.accessToken = response.authResponse.accessToken;
    				data.status = true;
    				callback(data);
    		  } else if (response.status === 'not_authorized') {
    			    callback(data);
    		  } else {
    			    callback(data);
    		  }
    		 });
    	};

        var getUserInfo = function(callback) {
          FB.api('/me', function(res) {
            var token =  FB.getAuthResponse().accessToken;
            $rootScope.$apply(function() {
              $rootScope.user = res;
              callback(res,token);
            });
          });
        };

    	var login = function(cb){
    		FB.login(function(response) {
    		    //let event system take care of login flow.
                console.log(response);
    		},{scope: 'email'});
    	};

        var logout = function(cb) {

          FB.logout(function(response) {
            $rootScope.$apply(function() {
    		  isLogggedOut = true;
              explicitLogOut = true;
    		  $http.defaults.headers.common.Authorization = '';
    		  $rootScope.authenticated = false;
      		  $rootScope.user = null;
      		  $auth.removeToken();
              $rootScope.$broadcast('onLogoutComplete',{});
    		  cb();
            });
          });
        };

    	var getIsLoggedOut = function(){
    		return isLogggedOut;
    	};

        var getExplicitLogOut = function(){
            return explicitLogOut;
        };


        return {
          watchLoginChange: watchLoginChange,
          getUserInfo: getUserInfo,
    	  getLoginStatus:getLoginStatus,
          logout: logout,
    	  login: login,
    	  sdkLoaded: sdkLoaded,
          fbinit: fbinit,
    	  getIsLoggedOut : getIsLoggedOut,
          getExplicitLogOut : getExplicitLogOut
        };
      });

})();
