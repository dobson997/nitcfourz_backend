<?php

use App\Models\Team;

class TeamTest extends TestCase {

	public $api_base = "/api/team/";
	protected $isDebugEnabled = false;

	function __construct(){

	}


	public function sample(){
		return  [

				'teamname' => str_random(10),

		];
	}

	public function get(){
		return Team::get()->first();
	}

	public function create($content){
		return Team::create($content);
	}

	public function testRouteAll(){

		$server = $this->token();

		$response = $this->call('GET',$this->api_base.'all', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());

	}

	public function testRouteCreate(){
		$content = $this->sample();
		$server = $this->token();

		$response = $this->call('POST',$this->api_base.'create', $content, [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRouteGetId(){

		$server = $this->token();
		$model = $this->get();

		$response = $this->call('GET', $this->api_base.$model->id, [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRoutePostId(){

		$content = $this->sample();
		$server = $this->token();
		$model = $this->get();

		$response = $this->call('POST', $this->api_base.$model->id, $content, [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRoutePostDelete(){

		$content = $this->sample();
		$server = $this->token();
		$model = $this->create($content);

		$response = $this->call('POST', $this->api_base.$model->id.'/delete', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

}
