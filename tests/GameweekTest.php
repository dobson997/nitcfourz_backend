<?php

use App\Models\Gameweek;
use App\Models\Player;

class GameweekTest extends TestCase {

	public $api_base = "/api/gameweek/";
	protected $isDebugEnabled = false;

	function __construct(){

	}


	public function sample(){
		$player = Player::orderByRaw("RAND()")->get()->first();

		return  [

				'gameweek' => rand(1,100),

				'playerid' => $player->id,

				'goals' => rand(1,10),

				'cleansheet' => rand(1,10),

				'save' => rand(1,10),

				'assist' => rand(1,10),

				'redcard' => rand(1,10),

				'yellowcard' => rand(1,10),

				'penaltysaved' => rand(1,10),

				'penaltymissed' => rand(1,10),

				'hasplayed' => 1

		];
	}

	public function get(){
		return Gameweek::orderByRaw("RAND()")->get()->first();
	}

	public function create($content){
		return Gameweek::create($content);
	}

	public function testRouteAll(){

		$server = $this->token();

		$response = $this->call('GET',$this->api_base.'all', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());

	}

	public function testRouteCreate(){

		$content = $this->sample();
		$server = $this->token();

		$response = $this->call('POST',$this->api_base.'create', $content, [], [], $server);
		$json = $this->json($response);

        Gameweek::destroy($json['id']);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRouteGetId(){

		$server = $this->token();
		$model = $this->get();

		$response = $this->call('GET', $this->api_base.$model->id, [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRoutePostId(){

        $content = $this->sample();
		$server = $this->token();
		$model = $this->create($content);
        $update = $this->sample();

		$response = $this->call('POST', $this->api_base.$model->id, $update, [], [], $server);
		$json = $this->json($response);
        $model->delete();
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRoutePostDelete(){

		$content = $this->sample();
		$server = $this->token();
		$model = $this->create($content);

		$response = $this->call('POST', $this->api_base.$model->id.'/delete', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

}
