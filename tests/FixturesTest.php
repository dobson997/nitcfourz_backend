<?php

use App\Models\Fixtures;
use App\Models\Team;

class FixturesTest extends TestCase {

	public $api_base = "/api/fixtures/";
	protected $isDebugEnabled = false;

	function __construct(){

	}


	public function sample(){
        $team1 = Team::orderByRaw("RAND()")->get()->first();
        $team2 = Team::orderByRaw("RAND()")->get()->first();
		return  [

				'team1' => $team1->teamname,

				'goals1' => rand(1,10),

				'scorers1' => str_random(10),

				'team2' => $team2->teamname,

				'goals2' => rand(1,10),

				'scorers2' => str_random(10),

				'date' => '2017-01-01',

				'time' => '2017-01-01 11:00:00',

		];
	}

	public function get(){
		return Fixtures::orderByRaw("RAND()")->get()->first();
	}

	public function create($content){
		return Fixtures::create($content);
	}

	public function testRouteAll(){

		$server = $this->token();

		$response = $this->call('GET',$this->api_base.'all', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());

	}

	public function testRouteCreate(){
		$content = $this->sample();
		$server = $this->token();

		$response = $this->call('POST',$this->api_base.'create', $content, [], [], $server);
		$json = $this->json($response);

        //delete the stuff created
        Fixtures::destroy($json['id']);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRouteGetId(){

		$server = $this->token();
		$model = $this->get();

		$response = $this->call('GET', $this->api_base.$model->id, [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRoutePostId(){

		$content = $this->sample();
		$server = $this->token();
		$model = $this->create($content);
        $update = $this->sample();

		$response = $this->call('POST', $this->api_base.$model->id, $update, [], [], $server);
		$json = $this->json($response);
        $model->delete();
		$this->assertEquals(200, $response->getStatusCode());

	}

	public function testRoutePostDelete(){

		$content = $this->sample();
		$server = $this->token();
		$model = $this->create($content);

		$response = $this->call('POST', $this->api_base.$model->id.'/delete', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

}
