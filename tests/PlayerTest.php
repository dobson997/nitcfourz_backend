<?php

use App\Models\Player;
use App\Models\Team;

class PlayerTest extends TestCase {

	public $api_base = "/api/player/";
	protected $isDebugEnabled = false;

	function __construct(){

	}


	public function sample(){
        $team1 = Team::orderByRaw("RAND()")->get()->first();
		return  [

				'name' => str_random(10),

				'price' => 1000,

				'teamname' => $team1->teamname,

                'type' => 2,

				'fbid' => str_random(10),

		];
	}

	public function get(){
		return Player::orderByRaw("RAND()")->get()->first();
	}

	public function create($content){
		return Player::create($content);
	}

	public function testRouteAll(){

		$server = $this->token();

		$response = $this->call('GET',$this->api_base.'all', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());

	}

	public function testRouteCreate(){
		$content = $this->sample();
		$server = $this->token();

		$response = $this->call('POST',$this->api_base.'create', $content, [], [], $server);
		$json = $this->json($response);

        Player::destroy($json['id']);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRouteGetId(){

		$server = $this->token();
		$model = $this->get();

		$response = $this->call('GET', $this->api_base.$model->id, [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRoutePostId(){

        $content = $this->sample();
		$server = $this->token();
		$model = $this->create($content);
        $update = $this->sample();

		$response = $this->call('POST', $this->api_base.$model->id, $update, [], [], $server);
		$json = $this->json($response);

        $model->delete();

		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testRoutePostDelete(){

		$content = $this->sample();
		$server = $this->token();
		$model = $this->create($content);

		$response = $this->call('POST', $this->api_base.$model->id.'/delete', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

}
