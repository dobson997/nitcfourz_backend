<?php


require __DIR__.'/../bootstrap/autoload.php';

$app = require __DIR__.'/../bootstrap/app.php';

$app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();

use App\Models\User;
use App\Models\Team;
use App\Models\Player;
use App\Models\Fixtures;
use App\Models\Gameweek;
use App\Models\Playerhistory;
use App\Models\Status;
use App\Models\Temporary;

function getUser($name){
    return [
        'name' => $name,
        'email' => 'mail',
        'liquidcash' => 100000,
        'points' => 0,
        'rank' => 0,
        'fbid' => rand(10000,20000),
    ];

}

function getPlayer($name, $teamname, $type){
    return [
        'name' => $name,

        'price' => 500,

        'type' => $type,

        'teamname' => $teamname,

        'fbid' => '4',
    ];
}

function getFixture($t1, $t2){
    return [
        'team1' =>$t1 ,
        'goals1' => rand(1,10),
        'scorers1' => str_random(5).','.str_random(5),
        'team2' => $t2,
        'goals2' => rand(1,10),
        'scorers2' => str_random(5).','.str_random(5) ,
        'date' => '2017-01-01',
        'time' => '2017-01-01 11:00:00',
    ];
}

function getGameweek($gw, $pid){
    return [
        'gameweek' => $gw,

        'playerid' => $pid,

        'hasplayed' => 1,

        'goals' => 1,

        'save' => 1,

        'assist' => 1,

        'cleansheet' => 1,

        'yellowcard' => 1,

        'redcard' => 1,

        'penaltysaved' => 1,

        'penaltymissed' => 1
    ];
}

function getPlayerhistory($gw, $pid){
    return [
        'gameweek' => $gw,

        'userid' => $pid,

        'goalkeeper' => $pid,

        'player1' => $pid,

        'player2' => $pid,

        'player3' => $pid,

        'sub' => $pid,
    ];
}

function populate(){

    Status::create([
        'id'=> 1,
        'status' => '{"current_gw":"1","deadline_gw":"2017-02-05T00:00:00+05:30","transfer_deadline":"2017-02-05T00:00:00+05:30","launch_time":"2017-02-05T00:00:00+05:30","transfer_allowed":1,"game_state":2}'
    ]);

    $user1 = User::create(getUser('Awsin'));
    $user2 = User::create(getUser('Sameer'));
    $user3 = User::create(getUser('Vinay'));
    $user4 = User::create(getUser('Prasanth'));

    $team1 = Team::create(['teamname' => 'JFC']);
    $team2 = Team::create(['teamname' => 'AFC']);
    $team2 = Team::create(['teamname' => 'Hype Team']);
    $team2 = Team::create(['teamname' => 'Dinjho FC']);

    $player1 = Player::create(getPlayer('Sanath','JFC',1));
    $player2 = Player::create(getPlayer('Sabith','JFC',1));
    $player3 = Player::create(getPlayer('Chathan','JFC',1));
    $player4 = Player::create(getPlayer('Rinshad','JFC',1));
    $player5 = Player::create(getPlayer('Bagath','JFC',2));

    $player12 = Player::create(getPlayer('Jumbo','AFC',1));
    $player22 = Player::create(getPlayer('Ravi','AFC',1));
    $player32 = Player::create(getPlayer('Adi','AFC',1));
    $player42 = Player::create(getPlayer('Hsp','AFC',1));
    $player52 = Player::create(getPlayer('Kukkulu','AFC',2));

    $fixture = Fixtures::create(getFixture('JFC','AFC'));

    $gameweek = Gameweek::create(getGameweek(1,$player1->id));

    $playerhistory = Playerhistory::create(getPlayerhistory(1,$user1->id));

    $temporary = Temporary::create(getPlayerhistory(1,$user1->id));


}

populate();

?>
