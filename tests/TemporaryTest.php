<?php

use App\Models\Temporary;
use App\Models\Player;
use App\Models\User;

class TemporaryTest extends TestCase {

	public $api_base = "/api/temporary/";
	protected $isDebugEnabled = false;

	function __construct(){

	}


	public function sample(){
        $player = Player::orderByRaw("RAND()")->get()->first();
		$user = User::orderByRaw("RAND()")->get()->first();
		return  [

            'goalkeeper' => $player->id,

            'player1' => $player->id,

            'player2' => $player->id,

            'player3' => $player->id,

            'sub' => $player->id,

		];
	}

	public function get(){
		return Temporary::get()->first();
	}

	public function create($content){
		return Temporary::create($content);
	}

	public function testRouteAll(){

		$server = $this->token();

		$response = $this->call('GET',$this->api_base.'all', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());

	}

	public function testRouteCreate(){
		$content = $this->sample();
		$server = $this->token();

		$response = $this->call('POST',$this->api_base.'create', $content, [], [], $server);
		$json = $this->json($response);

        //Temporary::destroy($json['id']); cuz it returns a user!

		$this->assertEquals(400, $response->getStatusCode());
	}

	public function testRouteGetId(){

		$server = $this->token();
		$model = $this->get();

		$response = $this->call('GET', $this->api_base.$model->id, [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}


}
