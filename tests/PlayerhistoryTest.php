<?php

use App\Models\Playerhistory;
use App\Models\User;
use App\Models\Player;

class PlayerhistoryTest extends TestCase {

	public $api_base = "/api/playerhistory/";
	protected $isDebugEnabled = false;

	function __construct(){

	}


	public function sample(){

		$player = Player::orderByRaw("RAND()")->get()->first();
		$user = User::orderByRaw("RAND()")->get()->first();

		return  [

				'goalkeeper' => $player->id,

				'player1' => $player->id,

				'player2' => $player->id,

				'player3' => $player->id,

				'sub' => $player->id,

		];
	}

	public function get(){

		return Playerhistory::get()->first();
	}

	public function create($content){
		return Playerhistory::create($content);
	}

	public function testRouteAll(){

		$server = $this->token();

		$response = $this->call('GET',$this->api_base.'all', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());

	}

	public function testRouteGetId(){

		$server = $this->token();
		$model = $this->get();

		$response = $this->call('GET', $this->api_base.$model->id, [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}

}
