<?php

use App\Models\User;

class UserTest extends TestCase {

	public $api_base = "/api/user/";
	protected $isDebugEnabled = false;

	function __construct(){

	}


	public function sample(){
		return  [

				'name' => str_random(10),

				'email' => str_random(10)."@gmail.com",

				'liquidcash' => 10000,

				'points' => 0,

				'rank' => -1,

				'fbid' => str_random(10),

		];
	}

	public function get(){
		return User::get()->first();
	}

	public function create($content){
		return User::create($content);
	}

	public function testRouteAll(){

		$server = $this->token();

		$response = $this->call('GET',$this->api_base.'all', [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());

	}


	public function testRouteGetId(){

		$server = $this->token();
		$model = $this->get();

		$response = $this->call('GET', $this->api_base.$model->id, [], [], [], $server);
		$json = $this->json($response);

		$this->assertEquals(200, $response->getStatusCode());
	}


}
