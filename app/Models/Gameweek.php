<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Gameweek extends Model{

    protected $table = 'gameweek';
    protected $primaryKey = 'id';
    protected $fillable = ['gameweek', 'playerid', 'hasplayed', 'goals', 'cleansheet', 'save', 'assist', 'redcard', 'yellowcard', 'penaltymissed', 'penaltysaved', 'total' ];
    protected $hidden = [];
    public $timestamps = false;

    public function player(){

        return $this->belongsTo('App\Models\Player', 'playerid', 'id');
        
    }


/*
    public function getTimeAttribute($value){
        $val = Carbon::createFromFormat('Y-m-d H:i:s', $value,'Asia/Calcutta')->toIso8601String();
        return $val;
    }


    public function setTimeAttribute($value){

      $this->attributes['time'] = Carbon::parse($value)->toDateTimeString();

    }

    public function relation(){
        return $this->belongsTo('App\Models\', 'local', 'global');
    }

*/



}
