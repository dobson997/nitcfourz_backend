<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Fixtures extends Model{

    protected $table = 'fixtures';
    protected $primaryKey = 'id';
    protected $fillable = ['team1', 'goals1', 'scorers1', 'team2', 'goals2', 'scorers2', 'date','time', ];
    protected $hidden = [];
    public $timestamps = false;

    public function getTimeAttribute($value){
        $val = Carbon::createFromFormat('Y-m-d H:i:s', $value,'Asia/Calcutta')->toIso8601String();
        return $val;
    }


    public function setTimeAttribute($value){

      $this->attributes['time'] = Carbon::parse($value)->toDateTimeString();

    }

    public function getDateAttribute($value){
        $val = Carbon::createFromFormat('Y-m-d', $value,'Asia/Calcutta')->toIso8601String();
        return $val;
    }


    public function setDateAttribute($value){

      $this->attributes['date'] = Carbon::parse($value)->toDateString();

    }







}
