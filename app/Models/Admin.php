<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\User;

class Admin extends Model{

    protected $table = 'admins';
    protected $primaryKey = 'fbid';
    protected $fillable = ['fbid'];
    protected $hidden = [];
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\Models\User', 'fbid', 'fbid');
    }

/*
    public function getTimeAttribute($value){
        $val = Carbon::createFromFormat('Y-m-d H:i:s', $value,'Asia/Calcutta')->toIso8601String();
        return $val;
    }


    public function setTimeAttribute($value){

      $this->attributes['time'] = Carbon::parse($value)->toDateTimeString();

    }

    public function relation(){
        return $this->belongsTo('App\Models\', 'local', 'global');
    }

*/



}
