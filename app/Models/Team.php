<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Team extends Model{

    protected $table = 'teams';
    protected $primaryKey = 'id';
    protected $fillable = ['teamname', ];
    protected $hidden = [];
    public $timestamps = false;

    public function players(){
        return $this->hasMany('App\Models\Player', 'teamname', 'teamname');
    }

/*
    public function getTimeAttribute($value){
        $val = Carbon::createFromFormat('Y-m-d H:i:s', $value,'Asia/Calcutta')->toIso8601String();
        return $val;
    }


    public function setTimeAttribute($value){

      $this->attributes['time'] = Carbon::parse($value)->toDateTimeString();

    }

    public function relation(){
        return $this->belongsTo('App\Models\', 'local', 'global');
    }

*/



}
