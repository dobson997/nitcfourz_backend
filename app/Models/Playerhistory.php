<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Playerhistory extends Model{

    protected $table = 'playerhistory';
    protected $primaryKey = 'id';
    protected $fillable = ['gameweek', 'userid', 'goalkeeper', 'player1', 'player2', 'player3', 'sub', 'total'];
    protected $hidden = [];
    public $timestamps = false;

/*
    public function getTimeAttribute($value){
        $val = Carbon::createFromFormat('Y-m-d H:i:s', $value,'Asia/Calcutta')->toIso8601String();
        return $val;
    }


    public function setTimeAttribute($value){

      $this->attributes['time'] = Carbon::parse($value)->toDateTimeString();

    }

    public function relation(){
        return $this->belongsTo('App\Models\', 'local', 'global');
    }

*/



}
