<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Player;

class Temporary extends Model{

    protected $table = 'temporary';
    protected $primaryKey = 'id';
    protected $fillable = ['userid', 'goalkeeper', 'player1', 'player2', 'player3', 'sub', ];
    protected $hidden = [];
    public $timestamps = false;

    public function goalkeeper(){
        return $this->belongsTo('App\Models\Player', 'goalkeeper', 'id');
    }
    public function player1(){
        return $this->belongsTo('App\Models\Player', 'player1', 'id');
    }
    public function player2(){
        return $this->belongsTo('App\Models\Player', 'player2', 'id');
    }
    public function player3(){
        return $this->belongsTo('App\Models\Player', 'player3', 'id');
    }
    public function sub(){
        return $this->belongsTo('App\Models\Player', 'sub', 'id');
    }

/*
    public function getTimeAttribute($value){
        $val = Carbon::createFromFormat('Y-m-d H:i:s', $value,'Asia/Calcutta')->toIso8601String();
        return $val;
    }


    public function setTimeAttribute($value){

      $this->attributes['time'] = Carbon::parse($value)->toDateTimeString();

    }

    public function relation(){
        return $this->belongsTo('App\Models\', 'local', 'global');
    }

*/



}
