<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Player extends Model{

    const TYPE_PLAYER = 1;
    const TYPE_GOALKEEPER = 2;

    protected $table = 'players';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'price', 'type', 'teamname', 'fbid', ];
    protected $hidden = [];
    public $timestamps = false;

/*
    public function getTimeAttribute($value){
        $val = Carbon::createFromFormat('Y-m-d H:i:s', $value,'Asia/Calcutta')->toIso8601String();
        return $val;
    }


    public function setTimeAttribute($value){

      $this->attributes['time'] = Carbon::parse($value)->toDateTimeString();

    }

    public function relation(){
        return $this->belongsTo('App\Models\', 'local', 'global');
    }

*/



}
