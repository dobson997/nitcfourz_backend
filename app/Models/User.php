<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Temporary;

class User extends Model implements AuthenticatableContract{

    use Authenticatable;

    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'email', 'liquidcash', 'points', 'rank', 'fbid', ];
    protected $hidden = ['remember_token'];
    public $timestamps = false;

    public function setTeam(){
        $team = Temporary::with(['goalkeeper','player1','player2','player3','sub'])
                        ->where('userid', $this->id)->get()->first();

        $this->team = $team;
    }

}
