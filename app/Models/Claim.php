<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Player;

class Claim extends Model{

    const TYPE_UNAPPROVED = 0;
    const TYPE_APPROVED = 1;

    protected $table = 'claim';
    protected $primaryKey = 'id';
    protected $fillable = ['userid', 'playerid', 'approve'];
    protected $hidden = [];
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\Models\User', 'userid', 'id');
    }

    public function player(){
        return $this->belongsTo('App\Models\Player', 'playerid', 'id');
    }

/*
    public function getTimeAttribute($value){
        $val = Carbon::createFromFormat('Y-m-d H:i:s', $value,'Asia/Calcutta')->toIso8601String();
        return $val;
    }


    public function setTimeAttribute($value){

      $this->attributes['time'] = Carbon::parse($value)->toDateTimeString();

    }

    public function relation(){
        return $this->belongsTo('App\Models\', 'local', 'global');
    }

*/



}
