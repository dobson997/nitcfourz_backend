<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\User;

class Shares extends Model{

    protected $table = 'shares';
    protected $primaryKey = 'id';
    protected $fillable = ['userid', 'team', ];
    protected $hidden = [];
    public $timestamps = false;


    public function user(){
        return $this->belongsTo('App\Models\User', 'userid', 'id');
    }


    public function getTeamAttribute($value){
        return  json_decode($value);
    }


    public function setTeamAttribute($value){
      $this->attributes['team'] = json_encode($value);
    }







}
