<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\GameStatus;

class GameStatusProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app->singleton('App\Services\GameStatus', function ($app) {
            return GameStatus::getStatus();
        });
	}

}
