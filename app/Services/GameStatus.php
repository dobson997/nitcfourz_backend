<?php namespace App\Services;

use Carbon\Carbon;
use App\Models\Status;

class GameStatus {

    const TYPE_ALLOWED = 1;
    const TYPE_NOTALLOWED = 0;

    //game states
    const TYPE_READY_TO_LAUNCH = 1;
    const TYPE_MAINTENANCE = 2;
    const TYPE_RUNNING = 3;
    const TYPE_GAME_ENDED = 4;

    public $current_gw;
    public $deadline_gw;
    public $transfer_deadline;
    public $transfer_allowed;
    public $game_state;
    public $launch_time;

    public function __construct(){

    }

    public function load($status){
        $json = json_decode($status->status, TRUE);
        foreach ($json as $key => $value) {
                $this->{$key} = $value; //easy peasy.
        }
    }

    public static function getStatus(){
        $status = Status::find(1);
        if(!$status){
            $status = new GameStatus();
            return $status;
        }
        $gs = new GameStatus();
        $gs->load($status);
        return $gs;
    }

    public function change($data){
        foreach ($data as $key => $value) {
            if(property_exists($this,$key)){
                 $this->{$key} = $value;
            }
        }
    }

    public function save(){
        $obj = [];
        foreach ($this as $key => $value) {
           //print "$key => $value\n";
           $obj[$key] = $value;
       }
       $status = Status::find(1);
       if(!$status){
           $status = new Status();
           $status->id = 1;
       }

       $status->status = json_encode($obj);
       $status->save();
    }

    public function toArray(){
        $obj = [];
        foreach ($this as $key => $value) {
           $obj[$key] = $value;
       }
       return $obj;
    }

}
