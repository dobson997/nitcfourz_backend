<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Log;
use Carbon\Carbon;

class LogRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        Log::create([
            'userid' => Auth::user()->id,
            'endpoint' => $request->url(),
            'data' => json_encode($request->all()),
            'time' => Carbon::now()->toDateTimeString()
        ]);


        return $next($request);
    }
}
