<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'shares'], function(){

		Route::post('search', 'Api\SharesController@search');

		Route::get('all', 'Api\SharesController@all');
		Route::post('create', 'Api\SharesController@create');
		Route::get('{id}','Api\SharesController@get');
		Route::post('{id}', 'Api\SharesController@update');
		Route::post('{id}/delete', 'Api\SharesController@delete');

	});
});
