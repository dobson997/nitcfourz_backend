<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'gameweek'], function(){

		Route::post('search', 'Api\GameweekController@search');
        Route::post('autofill', 'Api\GameweekController@autofill');

        Route::get('weeks', 'Api\GameweekController@uniqueGW');

		Route::get('all', 'Api\GameweekController@all');
		Route::get('player/{id}', 'Api\GameweekController@player');

		Route::post('create', 'Api\GameweekController@create');
		Route::get('{id}','Api\GameweekController@get');
		Route::post('{id}', 'Api\GameweekController@update');
		Route::post('{id}/delete', 'Api\GameweekController@delete');

	});
});
