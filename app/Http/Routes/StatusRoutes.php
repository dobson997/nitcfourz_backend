<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'status'], function(){


		Route::get('get','Api\StatusController@get');
		Route::post('update', 'Api\StatusController@update');

	});
});
