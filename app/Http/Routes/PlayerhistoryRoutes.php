<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'playerhistory'], function(){

		Route::post('search', 'Api\PlayerhistoryController@search');

		Route::get('weekpoints/{userid}/{weekid}', 'Api\PlayerhistoryController@weekpoints');

        Route::post('update' ,'Api\PlayerhistoryController@updateHistory');
        Route::post('copy' ,'Api\PlayerhistoryController@copyTemporary');

		Route::get('all', 'Api\PlayerhistoryController@all');
		Route::post('create', 'Api\PlayerhistoryController@create');
		Route::get('{id}','Api\PlayerhistoryController@get');
		Route::post('{id}', 'Api\PlayerhistoryController@update');
		Route::post('{id}/delete', 'Api\PlayerhistoryController@delete');

	});
});
