<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'player'], function(){

		Route::post('search', 'Api\PlayerController@search');
        Route::get('select', 'Api\PlayerController@select');
        Route::get('teams', 'Api\PlayerController@uniqueTeams');

		Route::get('all', 'Api\PlayerController@all');
		Route::get('groups', 'Api\PlayerController@groups');
		Route::post('create', 'Api\PlayerController@create');
		Route::get('{id}','Api\PlayerController@get');
		Route::post('{id}', 'Api\PlayerController@update');
		Route::post('{id}/delete', 'Api\PlayerController@delete');

	});
});
