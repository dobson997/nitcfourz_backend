<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'admin'], function(){

		Route::post('search', 'Api\AdminController@search');

		Route::get('all', 'Api\AdminController@all');
		Route::post('create', 'Api\AdminController@create');
		Route::get('{id}','Api\AdminController@get');
		Route::post('{id}/delete', 'Api\AdminController@delete');

	});
});
