<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'team'], function(){

		Route::post('search', 'Api\TeamController@search');

		Route::get('all', 'Api\TeamController@all');
		Route::post('create', 'Api\TeamController@create');
		Route::get('{id}','Api\TeamController@get');
		Route::post('{id}', 'Api\TeamController@update');
		Route::post('{id}/delete', 'Api\TeamController@delete');

	});
});
