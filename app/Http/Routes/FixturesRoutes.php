<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'fixtures'], function(){

		Route::post('search', 'Api\FixturesController@search');
		Route::get('days', 'Api\FixturesController@getUniqueDates');
		Route::get('fixture/{date}', 'Api\FixturesController@fixture');
		Route::get('all', 'Api\FixturesController@all');
		Route::post('create', 'Api\FixturesController@create');
		Route::get('{id}','Api\FixturesController@get');
		Route::post('{id}', 'Api\FixturesController@update');
		Route::post('{id}/delete', 'Api\FixturesController@delete');


	});
});
