<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'claim'], function(){

		Route::post('search', 'Api\ClaimController@search');

		Route::get('all', 'Api\ClaimController@all');
		Route::post('create', 'Api\ClaimController@create');

        Route::get('user', 'Api\ClaimController@getClaim');
        Route::post('user', 'Api\ClaimController@deleteClaim');

		Route::get('{id}','Api\ClaimController@get');
		Route::post('{id}', 'Api\ClaimController@update');
		Route::post('{id}/delete', 'Api\ClaimController@delete');

	});
});
