<?php


Route::group(['prefix' => 'api'], function(){

	Route::group(['prefix' => 'temporary'], function(){

		Route::post('search', 'Api\TemporaryController@search');

        Route::post('exchange', 'Api\TemporaryController@exchange');
        Route::post('shuffle', 'Api\TemporaryController@shuffle');

		Route::get('all', 'Api\TemporaryController@all');
		Route::post('create', 'Api\TemporaryController@create');
		Route::get('{id}','Api\TemporaryController@get');
		Route::post('{id}', 'Api\TemporaryController@update');
		Route::post('{id}/delete', 'Api\TemporaryController@delete');

	});
});
