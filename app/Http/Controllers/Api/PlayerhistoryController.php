<?php namespace App\Http\Controllers\Api;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Playerhistory;
use App\Models\Player;
use App\Models\Gameweek;
use App\Models\User;
use App\Models\Status;
use App\Models\Temporary;
use GameStatus;


class PlayerhistoryController extends Controller {

    private $paginate_count = 15;

    public function __construct(){


        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');


        if(!config('settings.debug')){
            $this->middleware('jwt.auth', []);
            $this->middleware('admin.check', ['only' =>['updateHistory', 'copyTemporary', 'updatePoints']]);
            $this->middleware('log', ['only' => ['updateHistory', 'copyTemporary', 'updatePoints']]);
        }
    }

	public function all(){
		return Playerhistory::paginate($this->paginate_count);
	}

    public function weekpoints($userid,$weekid){

        $playerset = Playerhistory::where('gameweek',$weekid)
                                    ->where('userid',$userid)
                                    ->get()->first();

        if (!$playerset){
            return response()->json(['errors' => 'Empty Player set'], 400);
        }
        //could it be null?

        $ids = [
                $playerset->goalkeeper,
                $playerset->player1,
                $playerset->player2,
                $playerset->player3,
                $playerset->sub
            ];

        $game_details = Gameweek::with('player')
                                ->where('gameweek',$weekid)
                                ->whereIn('playerid', $ids)
                                ->get();

        //is it to good to assume that
        // every player will have an entry for a gameweek??
        $keyed =  $game_details->reduce(function ($item, $game) {
                $item[$game['playerid']] = $game;
                return $item;
            });

        return ['game' => $keyed, 'player' => $playerset];

    }



    public function get($id){
        return Playerhistory::findOrFail($id);
    }

/*
	public function update($id, Request $request){
        $playerhistory =  Playerhistory::findOrFail($id);
        $playerhistory->update($request->all()); //Or ->save()
        return $playerhistory;

	}

    public function delete($id){
        if(Playerhistory::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }
*/

    public function search(Request $request){

        if($request->term==''){
            $result = Playerhistory::paginate($this->paginate_count);
        }else{
            $result = Playerhistory::where('id','=',$request->term)

                                    ->orWhere('gameweek','LIKE','%'.$request->term.'%')

                                    ->orWhere('userid','LIKE','%'.$request->term.'%')

                                    ->orWhere('goalkeeper','LIKE','%'.$request->term.'%')

                                    ->orWhere('player1','LIKE','%'.$request->term.'%')

                                    ->orWhere('player2','LIKE','%'.$request->term.'%')

                                    ->orWhere('player3','LIKE','%'.$request->term.'%')

                                    ->orWhere('sub','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                //'gameweek' => 'required',

                //'userid' => 'required',

                'goalkeeper' => 'required',

                'player1' => 'required',

                'player2' => 'required',

                'player3' => 'required',

                'sub' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }


    public function copyTemporary(GameStatus $gamestatus){
        $gameweek = $gamestatus->current_gw;

        //Don't how much time would this operation take!
        // Wait a sec!! We can find who played and who didn't. can't we?
        $query = "INSERT INTO playerhistory (gameweek, userid, goalkeeper, player1, player2, player3, sub)
                    SELECT $gameweek as gameweek, userid, goalkeeper, player1, player2, player3, sub FROM temporary; ";

        if(DB::statement($query)){
            return response()->json(['status' => 'success'], 200);
        }

        return response()->json(['status' => 'error'], 400);

    }

    public function updateHistory(GameStatus $gamestatus){

        $gameweek = $gamestatus->current_gw; //Find out the current Gameweek, only those records need to be updated

        $thebigquery = "UPDATE playerhistory as ph SET total =

CASE
	 WHEN (select sum(hasplayed) from gameweek where gameweek.gameweek=$gameweek and gameweek.playerid in (ph.player1, ph.player2, ph.player3)) = 3
	 THEN

	 ( SELECT SUM(total) as totalPoints
	FROM gameweek WHERE playerid IN
		(ph.goalkeeper, ph.player1, ph.player2, ph.player3)
    and gameweek.gameweek = $gameweek
    )

     WHEN (select hasplayed from gameweek where gameweek.gameweek=$gameweek and gameweek.playerid = ph.sub ) = 0
	 THEN

	 ( SELECT SUM(total) as totalPoints
	FROM gameweek WHERE playerid IN
		(ph.goalkeeper, ph.player1, ph.player2, ph.player3)
    and gameweek.gameweek = $gameweek
    )

    ELSE

	 ( SELECT SUM(total) as totalPoints
	FROM gameweek WHERE playerid IN
		(ph.goalkeeper, ph.player1, ph.player2, ph.player3, ph.sub)
    and gameweek.gameweek = $gameweek
    )

END WHERE ph.gameweek = $gameweek;
 ";

        /*DB::statement($thebigquery);
        return [
            'status' => 'success'
        ];*/

        if(DB::statement($thebigquery)){

            $this->updatePoints();

            return response()->json(['status' => 'success'], 200);
        }

        return response()->json(['status' => 'error'], 400);

    }

    public function updatePoints(){

        $query = 'UPDATE users u
INNER JOIN
(
   SELECT userid, SUM(total) as sum
   FROM playerhistory
   GROUP BY userid

) i ON u.id = i.userid

SET u.points = i.sum';


        if(DB::statement($query)){
            return response()->json(['status' => 'success'], 200);
        }

        return response()->json(['status' => 'error'], 400);
    }

}
