<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Gameweek;
use App\Models\Player;
use GameStatus;
use DB;

class GameweekController extends Controller {

    private $paginate_count = 15;

    public function __construct(){


        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');

        
        if(!config('settings.debug')){
            $this->middleware('jwt.auth', []);
            $this->middleware('admin.check', ['only' =>['create','update','delete', 'autofill']]);
            $this->middleware('log', ['only' => ['create', 'update', 'delete', 'autofill']]);
        }
        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');
    }

	public function all(){
		return Gameweek::paginate($this->paginate_count);
	}

    public function player($id){
		return Gameweek::where('playerid',$id)->get();
	}

    public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        $player = Player::findOrFail($data['playerid']);
        if ($player->type == Player::TYPE_GOALKEEPER){
            $data['total'] = $data['goals'] * 4 + ($data['save'] / 2) * 1;
        }
        else{
            $data['total'] = $data['goals'] * 3;
        }
        $data['total'] = $data['total'] + $data['hasplayed'] * 1 + $data['assist'] * 2
                    + $data['cleansheet'] * 5 + $data['penaltymissed'] * -5
                    + $data['penaltysaved'] * 7;
        return Gameweek::create($data);
    }

    public function get($id){
        return Gameweek::findOrFail($id);
    }

	public function update($id, Request $request){
        $gameweek =  Gameweek::findOrFail($id);
        $data = $request->all();
        $player = Player::findOrFail($data['playerid']);
        if ($player->type == Player::TYPE_GOALKEEPER){
            $data['total'] = $data['goals'] * 4 + ($data['save'] / 2) * 1;
        }
        else{
            $data['total'] = $data['goals'] * 3;
        }
        $data['total'] = $data['total'] + $data['hasplayed'] * 1 + $data['assist'] * 2
                    + $data['cleansheet'] * 5 + $data['penaltymissed'] * -5
                    + $data['penaltysaved'] * 7;
        // $data['total'] = $data['hasplayed'] * 2 + $data['goals'] * 3 + $data['assist'] * 2
        //             + ($data['save'] / 3) * 1 + $data['cleansheet'] * 5 + $data['penaltymissed'] * -5
        //             + $data['penaltysaved'] * 7;
        $gameweek->update($data); //Or ->save()
        return $gameweek;

	}

    public function delete($id){
        if(Gameweek::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = Gameweek::with('player')->paginate($this->paginate_count);
        }else{
            $result = Gameweek::with('player')

                                    ->Where('gameweek','LIKE','%'.$request->term.'%')

                                    //->orWhere('playerid','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    public function uniqueGW(){
        return Gameweek::distinct('gameweek')->orderBy('gameweek', 'desc')->get(['gameweek']);
    }

    public function autofill(GameStatus $gamestatus){
        $gameweek = $gamestatus->current_gw;

        $query = "INSERT INTO gameweek (gameweek, playerid, hasplayed, goals, cleansheet, save, assist, redcard, yellowcard, total, penaltysaved, penaltymissed)
                    SELECT $gameweek as gameweek, id as playerid, 0 as hasplayed, 0 as goals,
                            0 as cleansheet, 0 as save, 0 as assist, 0 as redcard, 0 as yellowcard, 0 as total, 0 as penaltysaved, 0 as penaltymissed
                    FROM players
                    Where NOT EXISTS (SELECT 1 from gameweek where gameweek.playerid = players.id and gameweek.gameweek = $gameweek); ";

        if(DB::statement($query)){
            return response()->json(['status' => 'success'], 200);
        }

        return response()->json(['status' => 'error'], 400);
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'gameweek' => 'required',

                'playerid' => 'required',

                'goals' => 'required',

                'cleansheet' => 'required',

                'save' => 'required',

                'assist' => 'required',

                'redcard' => 'required',

                'yellowcard' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
