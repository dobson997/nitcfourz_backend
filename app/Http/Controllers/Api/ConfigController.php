<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Config;
use GameStatus;

class ConfigController extends Controller {



    public function __construct(){


        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');

        
        if(!config('settings.debug')){
            $this->middleware('jwt.auth', ['except' => []]);
            $this->middleware('admin.check', ['except' =>['config']]);
        }

    }

	public function config(GameStatus $gamestatus){
        $config = array_merge($gamestatus->toArray(),config('config.user'));
        return $config;
    }

    public function adminconfig(){
        return config('config.admin');
    }

}
