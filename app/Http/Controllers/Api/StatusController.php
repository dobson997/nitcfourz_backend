<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Status;
use GameStatus;


class StatusController extends Controller {

    private $paginate_count = 15;

    public function __construct(){


        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');

        
        if(!config('settings.debug')){
            $this->middleware('jwt.auth', []);
            $this->middleware('admin.check', ['only' =>['update']]);
            $this->middleware('log', ['only' => ['update']]);
        }

    }


    public function get(GameStatus $gamestatus){
        return $gamestatus->toArray();
    }

	public function update(Request $request, GameStatus $gamestatus){
        $gamestatus->change($request->all());
        $gamestatus->save();
        return $gamestatus->toArray();

	}

}
