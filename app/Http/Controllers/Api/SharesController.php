<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Shares;
use App\Models\User;
use App\Models\Temporary;
use Auth;

class SharesController extends Controller {

    private $paginate_count = 15;

    public function __construct(){

        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');

       if(!config('settings.debug')){
           $this->middleware('jwt.auth', []);
       }
    }
/*
	public function all(){
		return Shares::paginate($this->paginate_count);
	}

    */

    public function create(Request $request){

        $user = User::get()->first();//Auth::user();
        $user->setTeam();
        $team = $user->team;
        $team->fbid = $user->fbid;
        $team->username = $user->name;
        $data = [
            'userid' => $user->id,
            'team' => $team
        ];
        return Shares::create($data);
    }

    public function get($id){
        return Shares::findOrFail($id);
    }
/*
	public function update($id, Request $request){
        $shares =  Shares::findOrFail($id);
        $shares->update($request->all()); //Or ->save()
        return $shares;

	}

    public function delete($id){
        if(Shares::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = Shares::paginate($this->paginate_count);
        }else{
            $result = Shares::where('id','=',$request->term)

                                    ->orWhere('userid','LIKE','%'.$request->term.'%')

                                    ->orWhere('team','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [



        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
