<?php namespace App\Http\Controllers\Api;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Fixtures;

class FixturesController extends Controller {

    private $paginate_count = 15;

    public function __construct(){

        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');
        
        if(!config('settings.debug')){
            $this->middleware('jwt.auth', []);
            $this->middleware('admin.check', ['only' =>['create','update','delete',]]);
        }

    }

	public function all(){
		return Fixtures::paginate($this->paginate_count);
	}

    public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        return Fixtures::create($data);
    }

    public function get($id){
        return Fixtures::findOrFail($id);
    }

	public function update($id, Request $request){
        $fixtures =  Fixtures::findOrFail($id);
        $fixtures->update($request->all()); //Or ->save()
        return $fixtures;

	}

    public function delete($id){
        if(Fixtures::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = Fixtures::paginate($this->paginate_count);
        }else{
            $result = Fixtures::where('team1','LIKE','%'.$request->term.'%')

                                    ->orWhere('team2','LIKE','%'.$request->term.'%')

                                    ->orWhere('date','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    public function getUniqueDates(){
        return Fixtures::distinct('date')->orderBy('date', 'desc')->get(['date']);
    }

    public function fixture($date){
        $date = Carbon::parse($date)->toDateString();
        return Fixtures::where('date', '=', $date)->get();
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'team1' => 'required',

                'team2' => 'required',

                'date' => 'required',

                'time' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
