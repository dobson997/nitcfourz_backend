<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Claim;
use App\Models\User;
use App\Models\Player;
use Auth;

class ClaimController extends Controller {

    private $paginate_count = 15;

    public function __construct(){

        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');


        if(!config('settings.debug')){
            $this->middleware('jwt.auth', []);
            $this->middleware('admin.check', ['except' =>['create', 'getClaim']]);
        }

    }

	public function all(){
		return Claim::paginate($this->paginate_count);
	}

    public function getClaim(){
        $id = Auth::user()->id;
        $result = Claim::with(['user','player'])
                                ->where('userid', $id)->get()->first();
        if($result){
            return $result;
        }
        return response()->json(['errors' => ['haven\'t claimed yet.']], 400);
    }

    public function deleteClaim(){
        $id = Auth::user()->id;
        $result = Claim::with(['user','player'])
                                ->where('userid', $id)->get()->first();

        if(!$result){
            return response()->json(['status' => 'error'], 400);
        }

        if(Claim::destroy($result->id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        $data['userid'] =  Auth::user()->id; //User::get()->first()->id;//
        $data['approve'] = Claim::TYPE_UNAPPROVED;
        $claim = Claim::create($data);

        $c = Claim::with(['user','player'])
                                ->where('userid', $claim->userid)->get()->first();

        return $c;
    }

    public function get($id){
        return Claim::findOrFail($id);
    }

	public function update($id, Request $request){
        $claim =  Claim::with('user')->findOrFail($id);
        $data = $request->all();
        $claim->update($data);

        if(isset($data['approve'])){
            $player = Player::findOrFail($claim->playerid);
            if($data['approve']==Claim::TYPE_UNAPPROVED){
                $player->fbid = NULL;
            }else{
                $player->fbid = $claim->user->fbid;
            }
            $player->save();
            $claim->player = $player;
        }


        return $claim;

	}

    public function delete($id){
        if(Claim::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = Claim::with(['user','player'])->paginate($this->paginate_count);
        }else{
            $result = Claim::with(['user','player'])

                                    ->where('approve','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'playerid' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
