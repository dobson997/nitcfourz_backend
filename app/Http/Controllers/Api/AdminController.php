<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Admin;

class AdminController extends Controller {

    private $paginate_count = 15;

    public function __construct(){


        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');


        if(!config('settings.debug')){
            $this->middleware('jwt.auth', []);
            $this->middleware('admin.check');
            $this->middleware('log', ['only' => ['create', 'delete']]);
        }

    }

	public function all(){
		return Admin::with('user')->paginate($this->paginate_count);
	}

    public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        return Admin::create($data);
    }

    public function get($id){
        return Admin::with('user')->findOrFail($id);
    }

	/*public function update($id, Request $request){
        $admin =  Admin::findOrFail($id);
        $admin->update($request->all()); //Or ->save()
        return $admin;

	}*/

    public function delete($id){
        if(Admin::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = Admin::with('user')->paginate($this->paginate_count);
        }else{
            $result = Admin::with(['user' => function($query) use ($request){
                $query->where('fbid','LIKE','%'.$request->term.'%');
            }])->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'fbid' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
