<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Temporary;
use App\Models\Player;
use App\Models\User;
use Auth;
use DB;
use GameStatus;

class TemporaryController extends Controller {

    private $paginate_count = 15;

    public function __construct(){
        
        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');

        if(!config('settings.debug')){
            $this->middleware('jwt.auth', []);
            $this->middleware('log', ['only' => ['create', 'exchange', 'shuffle']]);
        }
    }

	public function all(){
		return Temporary::paginate($this->paginate_count);
	}

    public function create(Request $request){

        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }

        $data = $request->all();

        /*$players = DB::table('players')
                ->whereIn('id', [$data['goalkeeper'], $data['player1'], $data['player2'], $data['player3'], $data['sub']])
                ->get();*/

        $goalee = Player::findOrFail($data['goalkeeper']);

        //goalee check!
        if ($goalee->type != Player::TYPE_GOALKEEPER){
            return response()->json(['errors' => ['There must be a goalkeeper']], 400);
        }

        //get the rest of em
        $players = Player::whereIn('id',[
            $data['player1'], $data['player2'],
            $data['player3'], $data['sub']
        ])
        ->where('type', Player::TYPE_PLAYER)
        ->get();

        if($players->count() != 4){
            //oh ow! 2 Goalees!
            return response()->json(['errors' => ['cant have more than one goalkeeper']], 400);
        }

        $teams = [];
        $teams[] = $goalee->teamname;

        $playercost = 0;
        foreach ($players as $player) {
            $playercost += $player->price;
            if (in_array($player->teamname, $teams)){
                return response()->json([
                    'errors' => ['Only one player from one team'],
                    'players' => $players
                ], 400);
            }
            else{
                array_push($teams, $player->teamname);
            }
        }

        $user = Auth::user();

        //check the money!!
        $totalPlayerCost = $goalee->price + $playercost;
        if( $user->liquidcash < $totalPlayerCost ){
            return response()->json(['errors' => ['Oh Man! You don\'t have enough money']], 400);
        }

        //All Set!!

        $data['userid'] =  $user->id;

        try{
            DB::transaction(function() use($user,$data,$totalPlayerCost){

                Temporary::create($data);
                $user->liquidcash = $user->liquidcash - $totalPlayerCost;
                $user->save();
                $user->setTeam();

                return $user;

            });

        }catch(Exception $e){
            return response()->json(['errors' => ['Oh Man! Did you create already?']], 400);
        }

        return $user;

    }

    public function exchange(Request $request, GameStatus $gamestatus){
        //do all the checks!!
        //Check if deadline has passed
        $deadline = Carbon::parse($gamestatus->transfer_deadline);
        $now = Carbon::now();
        if ($deadline->lt($now)){
            return response()->json(['errors' => ['Transfer deadline over.']], 400);
        }

        $data = $request->all();
        $user = Auth::user();

        $currentPlayer = Player::findOrFail($data['current']);
        $newPlayer = Player::findOrFail($data['newplayer']);

        if($newPlayer->price > $currentPlayer->price + $newPlayer->price){
            return response()->json(['errors' => 'Oh Man! You don\'t have enough money'], 400);
        }

        if ($newPlayer->type != $currentPlayer->type){
            return response()->json(['errors' => 'Both the players must be of same type'], 400);
        }

        $temp = Temporary::where('userid',$user->id)->get()->first();

        //handle null
        //handle types
        switch ($currentPlayer->id) {
            case $temp->goalkeeper:
                $temp->goalkeeper = $newPlayer->id;
                break;
            case $temp->player1:
                $temp->player1 = $newPlayer->id;
                break;
            case $temp->player2:
                $temp->player2 = $newPlayer->id;
                break;
            case $temp->player3:
                $temp->player3 = $newPlayer->id;
                break;
            case $temp->sub:
                $temp->sub = $newPlayer->id;
                break;
        }

        $user->liquidcash = $user->liquidcash + $currentPlayer->price - $newPlayer->price;

        try{
            DB::transaction(function() use($user,$temp){

                $temp->save();
                $user->save();
                $user->setTeam();

                return $user;

            });

        }catch(Exception $e){
            return response()->json(['errors' => 'Oh Man! Did you create already?'], 400);
        }

        return $user; //unreachable btw

    }

    public function shuffle(Request $request,GameStatus $gamestatus){

        //Check if deadline has passed
        $deadline = Carbon::parse($gamestatus->deadline_gw);
        $now = Carbon::now();
        if ($deadline->lt($now)){
            return response()->json(['errors' => ['Transfer deadline over.']], 400);
        }


        $data = $request->all();
        $user = Auth::user();
        $temp = Temporary::where('userid',$user->id)->get()->first();

        $currentsub = $data['currentsub'];
        $newsub = $data['newsub'];

        $currentPlayer = Player::findOrFail($currentsub);
        $newPlayer = Player::findOrFail($newsub);

        if ($currentPlayer->type == Player::TYPE_GOALKEEPER || $newPlayer->type == Player::TYPE_GOALKEEPER){
            return response()->json(['errors' => 'Cannot sub goalkeeper for an outfield player'], 400);
        }

        switch ($newsub) {
            case $temp->player1:
                $temp->player1 = $currentsub;
                break;
            case $temp->player2:
                $temp->player2 = $currentsub;
                break;
            case $temp->player3:
                $temp->player3 = $currentsub;
                break;
        }
        $temp->sub = $newsub;
        $temp->save();
        $user->setTeam();
        $user->zd = $data;
        $user->zt = $temp;

        return $user;
    }

    public function get($id){
        return Temporary::findOrFail($id);
    }

/*	public function update($id, Request $request){
        $temporary =  Temporary::findOrFail($id);
        $temporary->update($request->all()); //Or ->save()
        return $temporary;

	}*/
/*
    public function delete($id){
        if(Temporary::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }
*/
    public function search(Request $request){

        if($request->term==''){
            $result = Temporary::paginate($this->paginate_count);
        }else{
            $result = Temporary::where('id','=',$request->term)

                                    ->orWhere('gameweek','LIKE','%'.$request->term.'%')

                                    ->orWhere('userid','LIKE','%'.$request->term.'%')

                                    ->orWhere('goalkeeper','LIKE','%'.$request->term.'%')

                                    ->orWhere('player1','LIKE','%'.$request->term.'%')

                                    ->orWhere('player2','LIKE','%'.$request->term.'%')

                                    ->orWhere('player3','LIKE','%'.$request->term.'%')

                                    ->orWhere('sub','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'goalkeeper' => 'required',

                'player1' => 'required',

                'player2' => 'required',

                'player3' => 'required',

                'sub' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
