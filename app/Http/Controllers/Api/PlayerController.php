<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Player;

use Underscore\Types\Arrays;
use DB;

class PlayerController extends Controller {

    private $paginate_count = 15;

    public function __construct(){


        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');

        if(!config('settings.debug')){
            $this->middleware('jwt.auth', []);
            $this->middleware('admin.check', ['only' =>['create','update','delete',]]);
            $this->middleware('log', ['only' => ['create', 'update', 'delete']]);
        }
    }

	public function all(){
		return Player::orderBy('teamname')->paginate($this->paginate_count);
	}

    public function select(){
		return Player::orderBy('teamname')->get(['id','name','fbid','teamname']);
	}

    public function groups(){
        $players = Player::all();
        return $players->groupBy('teamname');
    }

    public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        return Player::create($data);
    }

    public function get($id){
        //possible injection here ! yikes
        $query = "Select id,name,price,type,teamname,fbid,  COALESCE((select SUM(total) from gameweek where playerid = '$id'),0) as total from players where id = '$id';";
        $player = DB::select($query);
        $r = json_decode(json_encode($player), true);
        return $r[0];
    }

	public function update($id, Request $request){
        $player =  Player::findOrFail($id);
        $player->update($request->all()); //Or ->save()
        return $player;

	}

    public function delete($id){
        if(Player::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = Player::orderBy('price', 'DESC')->paginate($this->paginate_count);
        }else{
            $result = Player::orderBy('price', 'DESC')->where('id','=',$request->term)

                                    ->orWhere('name','LIKE','%'.$request->term.'%')

                                    ->orWhere('teamname','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    public function uniqueTeams(){
        return Player::distinct('teamname')->orderBy('teamname', 'asc')->get(['teamname']);
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'name' => 'required',

                'price' => 'required',

                'type' => 'required',

                'teamname' => 'required'

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }


}
