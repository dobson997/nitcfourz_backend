<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\Team;

class TeamController extends Controller {

    private $paginate_count = 15;

    public function __construct(){

        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');

        if(!config('settings.debug')){
            $this->middleware('jwt.auth', []);
            $this->middleware('admin.check', ['only' =>['create','update','delete',]]);
            $this->middleware('log', ['only' => ['create', 'update', 'delete']]);
        }

    }

	public function all(){
		return Team::all();
	}

    public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        return Team::create($data);
    }

    public function get($id){
        return Team::with('players')->findOrFail($id);
    }

	public function update($id, Request $request){
        $team =  Team::findOrFail($id);
        $team->update($request->all()); //Or ->save()
        return $team;

	}

    public function delete($id){
        if(Team::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }

    public function search(Request $request){

        if($request->term==''){
            $result = Team::paginate($this->paginate_count);
        }else{
            $result = Team::where('id','=',$request->term)

                                    ->orWhere('teamname','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'teamname' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
