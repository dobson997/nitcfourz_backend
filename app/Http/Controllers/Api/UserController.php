<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;

class UserController extends Controller {

    private $paginate_count = 15;

    public function __construct(){

        date_default_timezone_set('Asia/Calcutta');
        setlocale(LC_MONETARY, 'en_IN');
        
        if(!config('settings.debug')){
            $this->middleware('jwt.auth', []);
        }
    }

	public function all(){
		return User::orderBy('points','DESC')->paginate($this->paginate_count);
	}

    public function select(){
        return User::get(['id','name','fbid']);
    }

    /*public function create(Request $request){
        $validator = $this->validateRequest($request);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $data = $request->all();
        return User::create($data);
    }*/

    public function get($id){
        return User::findOrFail($id);
    }

	/*public function update($id, Request $request){
        $user =  User::findOrFail($id);
        $user->update($request->all()); //Or ->save()
        return $user;

	}

    public function delete($id){
        if(User::destroy($id)){
            return response()->json(['status' => 'success'], 200);
        }else{
            return response()->json(['status' => 'error'], 400);
        }
    }*/

    public function search(Request $request){

        if($request->term==''){
            $result = User::paginate($this->paginate_count);
        }else{
            $result = User::where('id','=',$request->term)

                                    ->orWhere('name','LIKE','%'.$request->term.'%')

                                    ->orWhere('email','LIKE','%'.$request->term.'%')

                                    ->orWhere('fbid','LIKE','%'.$request->term.'%')

                                    ->paginate($this->paginate_count);
        }

        return $result;
    }

    /* Helpers */

    private function validateRequest($request){
        $rules = [

                'name' => 'required',

                'email' => 'required',

                'liquidcash' => 'required',

                'points' => 'required',

                'rank' => 'required',

                'fbid' => 'required',

        ];
        $validator = Validator::make($request->all(), $rules);
        return $validator;
    }

}
