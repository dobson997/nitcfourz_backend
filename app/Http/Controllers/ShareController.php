<?php namespace App\Http\Controllers;


use File;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Team;
use App\Models\Shares;

class ShareController extends Controller {

	public function __construct()
	{
		$this->middleware('guest');
	}

	public function team($id) {

        $team = Team::findOrFail($id);

		return view('share.team',['title'=> $team->teamname, 'id'=>$id]);
	}



    public function myteam($id) {
        $share = Shares::with('user')->findOrFail($id);
        $team = $share->team;
		return view('share.myteam',['title'=> $team->username.'\'s Team', 'id'=>$id]);
	}

    private function fetchImage($fbid, $path, $size=154){
        $url = 'http://graph.facebook.com/'.$fbid.'/picture?width=154&height=154';

        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        if(file_exists($path)){
            unlink($path); //push dummy change!
        }
        $data = file_get_contents($url, false, stream_context_create($arrContextOptions));
        file_put_contents($path, $data);
    }

    public function myteamimage($id){
        $share = Shares::with('user')->findOrFail($id);
        $team = $share->team;

        $paths = [];

        $img = Image::make(url('images/myteam.jpg'));


        $players = [
            $team->goalkeeper,
            $team->player1,
            $team->player2,
            $team->player3,
            $team->sub
        ];


        $coordiantes = [
            [42,362],
            [282,362],
            [521,362],
            [761,362],
            [1000,362]
        ];

        foreach ($players as $key => $player) {

            if($player->fbid){
                $fbid =  $player->fbid;
                $path = public_path()."/images/team/$fbid.jpg";
                $this->fetchImage($fbid, $path);
                $fb = Image::make($path);
                $img->insert($fb,'top-left',$coordiantes[$key][0],$coordiantes[$key][1]);
                unlink($path);
            }

            $text = $player->name;

            $img->text($text, $coordiantes[$key][0]+77, $coordiantes[$key][1]+200, function($font) {
                $font->file(public_path('fonts/SourceSansPro.ttf'));
                $font->size(20);
                $font->color('#000');
                $font->align('center');
                $font->valign('center');
            });

        }

        $fbid =  $team->fbid;
        $path = public_path()."/images/team/$fbid.jpg";
        $this->fetchImage($fbid, $path);
        $fb = Image::make($path);
        $img->insert($fb,'top-left',520,53);
        unlink($path);

        $text = $team->username.'\'s Team';

        $img->text($text, 520+77, 53+200, function($font) {
            $font->file(public_path('fonts/SourceSansPro.ttf'));
            $font->size(40);
            $font->color('#FFF');
            $font->align('center');
            $font->valign('center');
        });


        return $img->response('jpg', 70);



    }

    public function teamimage($id){

        $team = Team::with('players')->findOrFail($id);

        $paths = [];

        $img = Image::make(url('images/team.jpg'));

        $coordiantes = [
            [160,114],
            [501,114],
            [843,114],
            [160,382],
            [501,382],
            [843,382]
        ];

        foreach ($team->players as $key => $player) {

            if($player->fbid){
                $fbid =  $player->fbid;
                $path = public_path()."/images/team/$fbid.jpg";
                $this->fetchImage($fbid, $path);
                $fb = Image::make($path);
                $img->insert($fb,'top-left',$coordiantes[$key][0],$coordiantes[$key][1]);
                unlink($path);
            }

            $text = $player->name;

            $img->text($text, $coordiantes[$key][0]+77, $coordiantes[$key][1]+185, function($font) {
                $font->file(public_path('fonts/SourceSansPro.ttf'));
                $font->size(20);
                $font->color('#000');
                $font->align('center');
                $font->valign('center');
            });

        }

        return $img->response('jpg', 70);
    }

}
