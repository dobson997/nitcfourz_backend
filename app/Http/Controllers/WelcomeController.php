<?php namespace App\Http\Controllers;

use GameStatus;

class WelcomeController extends Controller {

	public function __construct()
	{
		$this->middleware('guest');
	}

	public function index(GameStatus $gamestatus) {

        switch($gamestatus->game_state){
            case GameStatus::TYPE_MAINTENANCE :
                return view('maintenance');
                break;
            case GameStatus::TYPE_READY_TO_LAUNCH :
                return view('launch');
                break;
            case GameStatus::TYPE_RUNNING:
                return view('index');
                break;
        }
        // for Ready to launch or running!

		return view('comingsoon'); //let this be default!
	}

    public function awesome() {
		return view('index');
	}

    public function admin() {
		return view('admin');
	}

}
