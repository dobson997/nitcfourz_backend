<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\User;
use Validator;
use Hash;
use Config;
use App\Models\Temporary;
use App\Models\Admin;
use GameStatus;

class AuthController extends Controller {


	public function __construct(){
        $this->middleware('jwt.auth', ['except' => ['authenticate','fb_authenticate', 'admin_authenticate', 'me']]);
	}


    public function fb_authenticate(Request $request, GameStatus $gamestatus){

      $values = $request->all();
      $fb_token = $values['fb_token'];
      $res = @file_get_contents("https://graph.facebook.com/v2.8/me?access_token=$fb_token&fields=id,name,email&format=json");
      $data = json_decode($res,TRUE);

      if(!isset($data) || isset($data['error'])){
        return response()->json(['error' => 'invalid_token'], 401);
      }
      $fbid = $data['id'];

      $user = User::where('fbid',$fbid)->get()->first();

      if(!$user){
        //new fb user.
        $start_money = config('settings.start_money');
        if(!isset($data['email'])){
            $data['email'] = 'not provided';
        }
        $user = User::create(['name'=>$data['name'],'fbid'=>$fbid, 'liquidcash'=> $start_money,
                              'points'=>0, 'rank'=>0,
                               'email'=> $data['email']]);
      }

      try {
          if (! $token = JWTAuth::fromUser($user)) {
              return response()->json(['error' => 'invalid_credentials'], 401);
          }
      } catch (JWTException $e) {
          // something went wrong
          return response()->json(['error' => 'could_not_create_token'], 500);
      }


     $user->setTeam();
     $config = array_merge($gamestatus->toArray(),config('config.user'));

      return ['token'=> $token,
            'user' => $user,
            'config' => $config];


    }


    public function admin_authenticate(Request $request){

      $values = $request->all();
      $fb_token = $values['fb_token'];
      $res = @file_get_contents("https://graph.facebook.com/me?access_token=$fb_token");
      $data = json_decode($res,TRUE);

      if(!isset($data) || isset($data['error'])){
        return response()->json(['error' => 'invalid_token'], 401);
      }
      $fbid = $data['id'];

      $user = User::where('fbid',$fbid)->get()->first();
      $admin = Admin::where('fbid',$fbid)->get()->first();

      if(!$user||!$admin){
          return response()->json(['error' => 'invalid_admin'], 401);
      }


      try {
          if (! $token = JWTAuth::fromUser($user)) {
              return response()->json(['error' => 'invalid_credentials'], 401);
          }
      } catch (JWTException $e) {
          // something went wrong
          return response()->json(['error' => 'could_not_create_token'], 500);
      }

      return [
          'token'=> $token,
        'user' => $user,
        'config' => config('config.admin')
    ];


    }


    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function me(){

        $user = User::find(1);
        $user->setTeam();
        return $user;

    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return response()->json(compact('user'));
    }



	public function refresh(){
		$token = JWTAuth::getToken();
		if(!$token){
			return response()->json(['error' => 'no token!'], 401);
		}
		try{
			$token = JWTAUTH::refresh($token);
		}catch(TokenInvalidException $e){
			return response()->json(['error' => 'access denied.'], 401);
		}
		return response()->json(['user'=>Auth::user(),'token'=>$token,'config' => Config::get('config')]);
	}





}
