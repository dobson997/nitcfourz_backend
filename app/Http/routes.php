<?php

Route::get('/', 'WelcomeController@index');

Route::get('/awesome', 'WelcomeController@awesome');

Route::get('/admin', 'WelcomeController@admin');

Route::group(['prefix' => 'share'], function(){

    Route::get('team/{id}', 'ShareController@team');
    Route::get('myteam/{id}', 'ShareController@myteam');
    Route::get('image/team/{id}','ShareController@teamimage');
    Route::get('image/myteam/{id}','ShareController@myteamimage');

});

Route::group(['prefix' => 'api'], function(){

	/*Route::post('auth', 'Auth\AuthController@authenticate');
	Route::post('refreshtoken', 'Auth\AuthController@refresh');
	Route::post('password', 'Auth\AuthController@changepassword');*/

	Route::get('config', 'Api\ConfigController@config');
    Route::get('config/admin', 'Api\ConfigController@adminconfig');

	Route::post('authenticate', 'Auth\AuthController@authenticate');
    Route::get('authenticate/user', 'Auth\AuthController@getAuthenticatedUser');
    Route::post('fbauthenticate','Auth\AuthController@fb_authenticate');

    Route::get('me','Auth\AuthController@me'); // <-- for testing

    Route::group(['prefix' => 'admin'], function(){
        Route::post('authenticate','Auth\AuthController@admin_authenticate');
    });

});

require('Routes/TeamRoutes.php');
require('Routes/UserRoutes.php');
require('Routes/PlayerRoutes.php');
require('Routes/GameweekRoutes.php');
require('Routes/PlayerhistoryRoutes.php');
require('Routes/FixturesRoutes.php');
require('Routes/TemporaryRoutes.php');
require('Routes/StatusRoutes.php');
require('Routes/AdminRoutes.php');
require('Routes/SharesRoutes.php');
require('Routes/ClaimRoutes.php');
