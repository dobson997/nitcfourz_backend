<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFixturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fixtures', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('team1');
			$table->integer('goals1')->nullable();
			$table->string('scorers1')->nullable();
			$table->string('team2');
			$table->integer('goals2')->nullable();
			$table->string('scorers2')->nullable();
			$table->date('date');
			$table->timestamp('time');

            $table->foreign('team1')
 				   ->references('teamname')->on('teams')
 				   ->onDelete('cascade')
 				   ->onUpdate('cascade');

            $table->foreign('team2')
    				   ->references('teamname')->on('teams')
    				   ->onDelete('cascade')
    				   ->onUpdate('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fixtures', function(Blueprint $table)
		{
			Schema::drop('fixtures');
		});
	}

}
