<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameweekTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gameweek', function (Blueprint $table) {
		   $table->increments('id');
		   $table->integer('gameweek');
		   $table->integer('playerid')->unsigned();
		   $table->integer('hasplayed');
		   $table->integer('goals');
		   $table->integer('cleansheet');
		   $table->integer('save');
		   $table->integer('assist');
		   $table->integer('redcard');
		   $table->integer('yellowcard');
		   $table->integer('total');
		   $table->integer('penaltysaved');
		   $table->integer('penaltymissed');

		   $table->unique(['gameweek','playerid']);

		   $table->foreign('playerid')
				   ->references('id')->on('players')
				   ->onDelete('cascade')
				   ->onUpdate('cascade');

	   });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gameweek');
	}

}
