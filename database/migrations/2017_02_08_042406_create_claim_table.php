<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('claim', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('userid')->unique()->unsigned(); //only one claim per user!
			$table->integer('playerid')->unsigned();
            $table->integer('approve')->unsigned();
            $table->foreign('userid')
					->references('id')->on('users')
					->onDelete('cascade')
					->onUpdate('cascade');
            $table->foreign('playerid')
					->references('id')->on('players')
					->onDelete('cascade')
					->onUpdate('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('claim');
	}

}
