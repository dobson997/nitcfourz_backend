<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerhistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('playerhistory', function (Blueprint $table) {
		   $table->increments('id');
		   $table->integer('gameweek');
		   $table->integer('userid')->unsigned();

		   $table->integer('goalkeeper')->unsigned();
		   $table->integer('player1')->unsigned();
		   $table->integer('player2')->unsigned();
		   $table->integer('player3')->unsigned();
		   $table->integer('sub')->unsigned();

		   $table->integer('total')->nullable()->unsigned();


		   $table->unique(['gameweek','userid']);

		   $table->foreign('userid')
				   ->references('id')->on('users')
				   ->onDelete('cascade')
				   ->onUpdate('cascade');

		   $table->foreign('player1')
				   ->references('id')->on('players')
				   ->onDelete('cascade')
				   ->onUpdate('cascade');

		   $table->foreign('player2')
				   ->references('id')->on('players')
				   ->onDelete('cascade')
				   ->onUpdate('cascade');

		   $table->foreign('player3')
  				 ->references('id')->on('players')
  				 ->onDelete('cascade')
  				 ->onUpdate('cascade');

  		   $table->foreign('sub')
  				 ->references('id')->on('players')
  				 ->onDelete('cascade')
  				 ->onUpdate('cascade');

			$table->foreign('goalkeeper')
					 ->references('id')->on('players')
					 ->onDelete('cascade')
					 ->onUpdate('cascade');

	   });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('playerhistory');
	}

}
