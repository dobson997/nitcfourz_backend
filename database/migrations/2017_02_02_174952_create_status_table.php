<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('status', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('status');
			// $table->integer('current_gw');
			// $table->timestamp('deadline_gw');
			// $table->integer('transfer_allowed');
			// $table->timestamp('transfer_deadline');
			// $table->integer('game_status');
            // $table->integer('launch_time');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('status');
	}

}
