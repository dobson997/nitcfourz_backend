module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);



  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    phpunit: {
        classes: {
            dir: 'tests/'
        },
        options: {
            bin: 'vendor/bin/phpunit',
            bootstrap: 'bootstrap/autoload.php',
            colors: true,
            configuration : 'phpunit.xml',
            verbose : true
        }
    },


    watch: {
        options: {
          nospawn: false,
          keepalive: true
        },

        frontend_grunt: {
          files: ['Gruntfile.js'],
          tasks: ['phpunit']
        },

         php : {
          files: ['app/**/**.php', 'tests/**.php'],
          tasks: ['phpunit']
        }



    }
  });


  grunt.registerTask('default', ['watch']);

  grunt.registerTask('test', function (target) {
       grunt.task.run([
           'phpunit',
           'watch'
       ]);
   });



};
