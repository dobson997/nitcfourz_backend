<?php

use App\Models\Player;
use App\Services\GameStatus;

// This file is to hold all the application specific variables.

$common = [
    'playerTypes' => [
        Player::TYPE_PLAYER => 'Player',
        Player::TYPE_GOALKEEPER => 'Goal Keeper'
    ],

    'player' => [
        'types' => [
            ['option' => 'Player' , 'type' => Player::TYPE_PLAYER ],
            ['option' => 'Goal Keeper', 'type' => Player::TYPE_GOALKEEPER]
        ]
    ],
];

$user =   [

        'rules' => [
            'Selecting Initial Squad' => [
                'To join the game select a fantasy football squad of 5 players, consisting of: 1 Goalkeeper and 4 Outfield players.',
                'The total value of your initial squad must not exceed Rs. 25 million.',
                'You can select up to 1 player from a single team.'
            ],
            'Managing Your Squad' => [
                'From your 5 player squad, select 4 players by the Gameweek deadline to form your team.',
                'All your points for the Gameweek will be scored by these 4 players, however if one doesn\'t play they may be automatically substituted.'
            ],
            'Transfers' => [
                'After selecting your squad you can buy and sell players in the transfer market. Unlimited transfers can be made at no cost until your first deadline.',
                'After first deadline, transfers can only be made after end of each round ie end of first round, end of Round of 16 and so on.',
                'As semi-final and final will be played on the same day, no transfers will be allowed after semi-final.'
            ],
            'Scoring' => [
                'During the season, your fantasy football players will be allocated points based on their performance in the Premier League.',
                'For playing 1 point',
                'For a clean sheet 5 points',
                'For each goal scored by an outfield player 3 points',
                'For each goal scored by a goalkeeper 4 points',
                'For each assist 2 points',
                'For every 2 saves 1 point ',
                'For each penalty miss -5',
                'For each penalty save 7'
            ],
            'Auto Substitution' => [
                'For each gameweek, if any of your outfield player has not played, he will be automatically substituted by your substitute.',
                'No substitution will be allowed for the goalkeeper.'
            ],
            'Contact' => [
                'For any queries, mail us at nitcfourz@gmail.com.'
            ]

        ]
];

$admin = [

        'gameStates' => [
            GameStatus::TYPE_READY_TO_LAUNCH  => 'Ready to Launch',
            GameStatus::TYPE_MAINTENANCE  => 'Maintenance',
            GameStatus::TYPE_RUNNING  => 'Up and Running',
            GameStatus::TYPE_GAME_ENDED  => 'Game Ended',
        ],
        'game' => [
            'states' => [
                ['option' => 'Ready to Launch' , 'type' => GameStatus::TYPE_READY_TO_LAUNCH ],
                ['option' => 'Maintenance' , 'type' => GameStatus::TYPE_MAINTENANCE ],
                ['option' => 'Up and Running' , 'type' => GameStatus::TYPE_RUNNING ],
                ['option' => 'Game Ended' , 'type' => GameStatus::TYPE_GAME_ENDED ],
            ]
        ],
        'transferTypes' => [
                GameStatus::TYPE_ALLOWED => 'Allowed',
                GameStatus::TYPE_NOTALLOWED => 'Not Allowed'
        ],

        'transfer' => [
            'types' => [
                ['option' => 'Allowed', 'type' => GameStatus::TYPE_ALLOWED],
                ['option' => 'Not Allowed', 'type' => GameStatus::TYPE_NOTALLOWED]
            ]
        ],
];

return [
    'admin' => array_merge($admin,$common),
    'user'  => array_merge($user,$common),
];

 ?>
